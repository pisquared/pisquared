import os
import time

from flask_security import SQLAlchemyUserDatastore
from flask_security.utils import hash_password

import sensitive
from config import DATABASE_DIR
from webapp import app
from webapp.models.auth import Role, User
from webapp.store import db


def populate():
    os.makedirs(DATABASE_DIR, exist_ok=True)
    db.init_app(app)

    user_datastore = SQLAlchemyUserDatastore(db, User, Role)

    app.app_context().push()
    db.create_all()

    # TODO: Handle DST
    admin_role = Role.create(name="admin")
    admin_user = user_datastore.create_user(email=sensitive.ADMIN_EMAIL,
                                            password=hash_password(sensitive.ADMIN_PASSWORD),
                                            timezone=time.tzname[0],
                                            tz_offset_seconds=-time.timezone)
    admin_user.roles = [admin_role]
    db.session.add(admin_user)

    db.session.commit()


if __name__ == '__main__':
    populate()
