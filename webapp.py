import threading

from flatfile_syncer import main as flatfile_syncer_main
from webapp import app


def main():
    threading.Thread(target=flatfile_syncer_main, args=()).start()
    app.run(host="0.0.0.0", debug=True)


if __name__ == '__main__':
    main()
