import random
import re
import string
from datetime import timedelta, datetime

from dateutil import tz
from flask_limiter.util import get_ipaddr
from flask_login import current_user


def limiter_func():
    if current_user.is_authenticated:
        return current_user.id
    return get_ipaddr()


def camel_case_to_snake_case(name):
    """
    Convertes a CamelCase name to snake_case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def camel_case_to_train_case(name):
    """
    Convertes a CamelCase name to train-case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1-\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1-\2', s1).lower()


def generate_random_string(N):
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(N))


def get_user_tz(user):
    return tz.tzoffset(None, timedelta(seconds=user.tz_offset_seconds))


def to_user_tz(dt, user):
    tzinfo = get_user_tz(user)
    return dt.replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def get_now_user(user):
    """
    Gets the user specific time based on his timezone
    """
    tzinfo = get_user_tz(user)
    return datetime.utcnow().replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def get_today_user(user):
    """
    Gets the user specific beginning of today based on his timezone
    """
    now = get_now_user(user)
    return datetime(year=now.year, month=now.month, day=now.day, tzinfo=now.tzinfo)


def get_tomorrow_user(user):
    """
    Gets the user specific beginning of tomorrow based on his timezone
    """
    today = get_today_user(user)
    return today + timedelta(days=1)