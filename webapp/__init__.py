#!/usr/bin/env python
import os

from flask import Flask
from flask_compress import Compress
from flask_debugtoolbar import DebugToolbarExtension
from flask_limiter import Limiter
from flask_security import Security, SQLAlchemyUserDatastore
from flaskext.markdown import Markdown

import config
from webapp.utils import camel_case_to_train_case, limiter_func

app = Flask(__name__)

app.config['SECRET_KEY'] = 'super-secret'
app.config['SECURITY_PASSWORD_SALT'] = 'super-secret'

# override for prod
if os.environ.get("FLASK_WEBAPP_ENV") == "prod":
    import sensitive

    if sensitive.SECRET_KEY == "CHANGEME" \
            or sensitive.SECURITY_PASSWORD_SALT == "CHANGEME":
        raise Exception("Default security key detected, generate more secure ones.")
    app.config['DEBUG'] = False
    app.config['DEBUG_TB_ENABLED'] = False
    app.config['DEBUG_TB_PROFILER_ENABLED'] = False
    app.config['SECRET_KEY'] = sensitive.SECRET_KEY
    app.config['SECURITY_PASSWORD_SALT'] = sensitive.SECURITY_PASSWORD_SALT

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['WHOOSHEE_DIR'] = os.path.join(os.getcwd(), 'search_index')

app.config['CACHE_TYPE'] = 'simple'

# small bursts - s*4, m*2, h*1.5
app.config['RATELIMIT_DEFAULT'] = '4 per second, 120 per minute, 5400 per hour'

app.config['DEBUG_TB_ENABLED'] = False
app.config['DEBUG_TB_PROFILER_ENABLED'] = False
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

# Initialize other plugins
Compress(app)
DebugToolbarExtension(app)
Limiter(app, key_func=limiter_func)
Markdown(app,
         extensions=['markdown.extensions.fenced_code',
                     'markdown.extensions.codehilite',
                     'markdown.extensions.sane_lists',
                     'markdown.extensions.tables'],
         safe_mode='escape')


def init_app():
    from webapp.csrf_protect import csrf
    csrf.init_app(app)

    from webapp.store import db, migrate
    db.init_app(app)

    from webapp import models
    migrate.init_app(app, db)

    from webapp.search import whooshee
    whooshee.init_app(app)

    from webapp.models.auth import Role, User
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    Security(app, user_datastore)

    from flask_cache import Cache
    from flask_sqlalchemy_cache import FromCache

    cache = Cache(app)

    @app.login_manager.user_loader
    def _load_user(id=None):
        return User.query.options(FromCache(cache)).filter_by(id=id).first()

    from webapp.jinja_filters import register_filters
    register_filters(app)

    from webapp.views import client_bp
    from webapp.views.utils import JournalEntryViewController
    JournalEntryViewController().register_views(client_bp)
    app.register_blueprint(client_bp)


init_app()
