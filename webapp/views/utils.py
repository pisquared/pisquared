from datetime import datetime

from flask import request, flash, redirect, render_template
from flask_login import current_user, login_required
from inflector import English

from webapp import camel_case_to_train_case
from webapp.models import BlogPost
from webapp.store import db
from webapp.utils import camel_case_to_snake_case


class AccessDenied(Exception):
    pass


class ViewController(object):
    def __init__(self, model):
        self.model = model
        self.model_name = model.__name__
        self.model_name_s = camel_case_to_train_case(model.__name__)
        self.model_name_p = English().pluralize(self.model_name_s)
        self.model_name_s_u = camel_case_to_snake_case(model.__name__)
        self.model_name_p_u = English().pluralize(self.model_name_s_u)

    def get_template_ctx(self, instances, ctx):
        return dict()

    def process_create_args(self):
        return dict()

    def _get_view_func(self):
        @login_required
        def get_instances():
            instances = self.model.query.filter_by(user=current_user, archived=False).all()
            template_ctx = self.get_template_ctx(instances, ctx)
            return render_template("{}/{}.html".format(self.model_name_p, self.model_name_p),
                                   ctx=ctx,
                                   **template_ctx)

        return get_instances

    def _create_view_func(self):
        @login_required
        def create_instance():
            try:
                instance_create_args = self.process_create_args()
            except Exception as e:
                flash(str(e), category="error")
                return redirect(request.referrer)
            instance = self.model.create(log_class=Log,
                                         user=current_user,
                                         **instance_create_args)
            db.session.add(instance)
            db.session.commit()
            flash("{} created.".format(self.model_name))
            return redirect(request.referrer)

        return create_instance

    def _get_instance_by_id(self, instance_id):
        instance = self.model.query.filter_by(id=instance_id, user=current_user).first()
        if not instance:
            raise ("{} doesn't exist".format(self.model_name))
        return instance

    def _archive_instance_view_func(self):
        @login_required
        def archive_instance(instance_id):
            try:
                instance = self._get_instance_by_id(instance_id)
                now = datetime.utcnow()
                instance.archived = True
                instance.archived_ts = now
                db.session.add(instance)
                db.session.commit()
            except Exception as e:
                flash(e, category="error")
                return redirect(request.referrer)
            flash("{} archived.".format(self.model_name))
            return redirect(request.referrer)

        return archive_instance

    def _unarchive_instance_view_func(self):
        @login_required
        def unarchive_instance(instance_id):
            try:
                instance = self._get_instance_by_id(instance_id)
                instance.archived = False
                instance.archived_ts = None
                db.session.add(instance)
                db.session.commit()
            except Exception as e:
                flash(e, category="error")
                return redirect(request.referrer)
            flash("{} unarchived.".format(self.model_name))
            return redirect(request.referrer)

        return unarchive_instance

    def _delete_instance_view_func(self):
        @login_required
        def delete_instance(instance_id):
            try:
                instance = self._get_instance_by_id(instance_id)
                db.session.delete(instance)
                db.session.commit()
            except Exception as e:
                flash(e, category="error")
                return redirect(request.referrer)
            flash("{} deleted.".format(self.model_name))
            return redirect(request.referrer)

        return delete_instance

    def register_views(self, bp):
        url = "/{}".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_get'.format(name=self.model_name_p),
                        view_func=self._get_view_func())

        url = "/{}/create".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_create'.format(name=self.model_name_p),
                        view_func=self._create_view_func(), methods=['POST'])

        url = "/{}/<int:instance_id>/delete".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_delete'.format(name=self.model_name_p),
                        view_func=self._delete_instance_view_func())

        url = "/{}/<int:instance_id>/archive".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_archive'.format(name=self.model_name_p),
                        view_func=self._archive_instance_view_func())

        url = "/{}/<int:instance_id>/unarchive".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_unarchive'.format(name=self.model_name_p),
                        view_func=self._unarchive_instance_view_func())


class JournalEntryViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(BlogPost)
