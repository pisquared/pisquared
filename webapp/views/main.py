import os
from collections import OrderedDict

from flask import redirect, request, url_for, render_template, flash, send_from_directory
from flask_login import login_required
from flask_security import roles_required

from config import BLOG_POSTS_DIR
from webapp.models import BlogPost, get_random_quote
from webapp.views import client_bp


def get_dated_bps():
    blog_posts = BlogPost.get_all_by(order_by=BlogPost.created_ts.desc())
    dated_bp = OrderedDict()
    for bp in blog_posts:
        dated_bp.setdefault(bp.created_ts.year, OrderedDict()).setdefault(bp.created_ts.strftime("%B"), []).append(bp)
    return dated_bp, blog_posts


@client_bp.route('/')
def get_blog():
    dated_bp, blog_posts = get_dated_bps()
    random_quote = get_random_quote()
    return render_template('home.html',
                           dated_bp=dated_bp,
                           blog_posts=blog_posts,
                           random_quote=random_quote,
                           title="PiSquared Blog")


@client_bp.route('/<filename>')
def get_blog_single(filename):
    bp = BlogPost.get_one_by(filename=filename)
    blog_posts = [bp]
    dated_bp, _ = get_dated_bps()
    random_quote = get_random_quote()
    return render_template('home.html',
                           blog_posts=blog_posts,
                           dated_bp=dated_bp,
                           random_quote=random_quote,
                           title="PiSquared Blog")


@client_bp.route('/media/<path:path>')
def serve_media(path):
    return send_from_directory(os.path.join(BLOG_POSTS_DIR, 'media'), path)


@client_bp.route('/projects')
def get_projects():
    return render_template('projects.html',
                           title="PiSquared Projects")


@client_bp.route('/about_me')
def get_aboutme():
    return render_template('aboutme.html',
                           title="PiSquared About Me")


@client_bp.route('/blog_posts/new')
@login_required
@roles_required('admin')
def get_blog_posts_new():
    return render_template('blog_post_new.html',
                           title="Create blog post")


@client_bp.route('/blog_posts/create', methods=["POST"])
@login_required
@roles_required('admin')
def blog_post_create():
    body = request.form.get('body')
    BlogPost.create(body=body)
    flash("Blog post created")
    return redirect(url_for('client_bp.get_blog'))


@client_bp.route('/blog_posts/<int:instance_id>/edit')
@login_required
@roles_required('admin', )
def get_blog_post_edit(instance_id):
    try:
        bp = BlogPost.get_one_by(id=instance_id)
    except Exception as e:
        flash(str(e), category="error")
        return redirect(request.referrer)
    return render_template('blog_post_edit.html',
                           title="Edit blog post",
                           bp=bp)


@client_bp.route('/blog_posts/<int:instance_id>/update', methods=["POST"])
@login_required
@roles_required('admin')
def blog_post_update(instance_id):
    try:
        bp = BlogPost.get_one_by(id=instance_id)
    except Exception as e:
        flash(str(e), category="error")
        return redirect(request.referrer)
    body = request.form.get('body')
    bp.update(body=body)
    flash("Blog post created")
    return redirect(url_for('client_bp.get_blog'))


@client_bp.route('/blog_posts/<int:instance_id>/archive')
@login_required
@roles_required('admin')
def blog_post_archive(instance_id):
    try:
        bp = BlogPost.get_one_by(id=instance_id)
    except Exception as e:
        flash(str(e), category="error")
        return redirect(request.referrer)
    bp.archive()
    flash("Blog post archived")
    return redirect(url_for('client_bp.get_blog'))


@client_bp.route('/blog_posts/<int:instance_id>/unarchive')
@login_required
@roles_required('admin')
def blog_post_unarchive(instance_id):
    try:
        bp = BlogPost.get_one_by(id=instance_id, archived=True)
    except Exception as e:
        flash(str(e), category="error")
        return redirect(request.referrer)
    bp.unarchive()
    flash("Blog post archived")
    return redirect(url_for('client_bp.get_blog'))


@client_bp.route('/blog_posts/<int:instance_id>/delete')
@login_required
@roles_required('admin')
def blog_post_delete(instance_id):
    try:
        bp = BlogPost.get_one_by(archived=True, id=instance_id)
    except Exception as e:
        flash(str(e), category="error")
        return redirect(request.referrer)
    return render_template("blog_post_delete_confirm.html",
                           bp=bp)


@client_bp.route('/blog_posts/<int:instance_id>/destroy', methods=['POST'])
@login_required
@roles_required('admin')
def blog_post_destroy(instance_id):
    try:
        bp = BlogPost.get_one_by(archived=True, id=instance_id)
    except Exception as e:
        flash(str(e), category="error")
        return redirect(request.referrer)
    bp.destroy()
    flash("Blog post deleted")
    return redirect(url_for('client_bp.get_blog'))


@client_bp.route('/archive')
@login_required
@roles_required('admin')
def get_archive():
    blog_posts = BlogPost.get_all_by(archived=True)
    dated_bp = OrderedDict()
    for bp in blog_posts:
        dated_bp.setdefault(bp.created_ts.date(), []).append(bp)
    return render_template('archived.html',
                           dated_bp=dated_bp)


@client_bp.route('/search')
def get_search_results():
    q = request.args.get('q')
    if not q:
        return render_template('search-results.html')

    blog_posts = BlogPost.query. \
        whooshee_search(q). \
        filter_by(archived=False).all()

    return render_template('search-results.html',
                           blog_posts=blog_posts,
                           q=q)
