import random

RANDOM_QUOTES = [
    "Injuries or meeting with aliens as a result of reading this blog are entirely international.",
    "Don't try writing blogs at home.",
    "Sometimes lun, sometimes win. - Hristo Stoichkov",
    "Please fasten your shoelaces before takeoff.",
    "In case of emergency, throw everything in the air and panic.",
    "You are not the winner of a lottery. Don't click anywhere.",
    "This message is brought to you by me.",
    "Read the prescription before applying.",
    "No peanuts were harmed during writing of this blog. Okay, maybe some peanuts but not a lot. Okay, maybe a lot. I should get in shape.",
    "Does not contain allergies. Made in an environment with allergies.",
    "Mind the gap between the blog post and your mouse.",
    "This is not the greatest blog in the world - this is just a tribute.",
    "I reject your reality and substitute my own. - Adam Savage",
    "Always stay cool like a swimming pool. - Rambo Amadeus",
    "The difficult I do immediately. The impossible takes a little longer. Unless I'm lazy. Then I think of lazier ways to do things.",
    "Life has no global meaning - you can only search for the next local maximum",
]


def get_random_quote():
    return random.choice(RANDOM_QUOTES)
