import random

from webapp.model_controller import ModelController, Ownable, Archivable, Datable
from webapp.search import whooshee
from webapp.store import db

blog_posts_tags = db.Table('blog_posts_tags',
                           db.Column('blog_post_id', db.Integer(), db.ForeignKey('blog_post.id')),
                           db.Column('tag_id', db.Integer(), db.ForeignKey('tag.id')))


@whooshee.register_model('body')
class BlogPost(db.Model, ModelController, Ownable, Datable, Archivable):
    filename = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    @property
    def title(self):
        try:
            return self.body.split('\n')[0].split('# ')[1]
        except Exception:
            return 'no title'

    @property
    def contents(self):
        try:
            return '\n'.join(self.body.split('\n')[1:])
        except Exception:
            return self.body


class Tag(db.Model, ModelController):
    name = db.Column(db.Unicode)
