from webapp.models.auth import User, Role
from webapp.models.blog_post import BlogPost, Tag
from webapp.models.random_quotes import get_random_quote
