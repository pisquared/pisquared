var textarea = document.querySelector('textarea');

if (textarea)
  textarea.addEventListener('keydown', autosize);

function autosize() {
  var el = this;
  setTimeout(function () {
    el.style.cssText = 'height:auto; padding:0';
    el.style.cssText = 'height:' + (el.scrollHeight + 30) + 'px';
  }, 0);
}

function setInboxEntry(promptWord) {
  document.getElementById('inbox-entry').innerHTML = promptWord;
  document.getElementById('inbox-entry').focus();
  return false;
}

function toggleArchiveEntry(uiElid) {
  var uiEl = document.getElementById(uiElid);
  uiEl.style.display = uiEl.style.display === 'none' ? '' : 'none';
}

window.onload = function () {
};