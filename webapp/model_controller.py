from datetime import datetime

from flask_login import current_user
from sqlalchemy.ext.declarative import declared_attr

from webapp.store import db
from webapp.utils import camel_case_to_snake_case


class ModelController(object):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def create(cls, **kwargs):
        now = datetime.utcnow()
        if issubclass(cls, Datable):
            if 'created_ts' not in kwargs:
                kwargs['created_ts'] = now
        # if issubclass(cls, Ownable):
        #     kwargs['user'] = current_user
        instance = cls(**kwargs)
        db.session.add(instance)
        db.session.commit()
        return instance

    def update(self, **kwargs):
        now = datetime.utcnow()
        if issubclass(self.__class__, Datable):
            kwargs['modified_ts'] = now
        for k, w in kwargs.items():
            setattr(self, k, w)
        db.session.add(self)
        db.session.commit()
        return self

    def archive(self):
        if issubclass(self.__class__, Archivable):
            now = datetime.utcnow()
            self.update(archived_ts=now, archived=True)
        return self

    def unarchive(self):
        if issubclass(self.__class__, Archivable):
            self.update(archived_ts=None, archived=False)
        return self

    def destroy(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @classmethod
    def _filter_method(cls, filters=None, **kwargs):
        if filters is None:
            filters = []
        if issubclass(cls, Archivable):
            kwargs['archived'] = kwargs.get('archived', False)

        order_by = kwargs.get("order_by", None)
        if order_by is not None:
            del kwargs['order_by']

        rv_method = filterings = cls.query.filter_by(**kwargs).filter(*filters)

        if order_by is not None:
            rv_method = filterings.order_by(order_by)
        return rv_method

    @classmethod
    def get_by(cls, first=0, last=None, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()[first:last]

    @classmethod
    def get_one_by(cls, filters=None, **kwargs):
        instance = cls._filter_method(filters, **kwargs).first()
        if not instance:
            raise Exception("{} doesn't exist.".format(cls.__name__))
        return instance

    @classmethod
    def get_all_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()


class Ownable(object):
    @declared_attr
    def user_id(self):
        return db.Column(db.Integer, db.ForeignKey('user.id'))

    @declared_attr
    def user(self):
        return db.relationship("User")


class Archivable(object):
    archived_ts = db.Column(db.DateTime())
    archived = db.Column(db.Boolean, default=False)


class Datable(object):
    created_ts = db.Column(db.DateTime())
    modified_ts = db.Column(db.DateTime())
