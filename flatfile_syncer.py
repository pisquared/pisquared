import asyncio
import datetime
import os

from watchgod import awatch, Change

from config import BLOG_POSTS_DIR
from webapp import app
from webapp.models import BlogPost

LOOPS = []
OBSERVERS = []


def register_observer(observer):
    OBSERVERS.append(observer)


def notify_observers(change):
    for c in change:
        change_type, path = c
        filename = os.path.basename(path)
        relpath = os.path.relpath(path, BLOG_POSTS_DIR)
        if filename.startswith('.'):
            continue
        if change_type == Change.added:
            handler = "file_added"
        elif change_type == Change.modified:
            handler = "file_modified"
        elif change_type == Change.deleted:
            handler = "file_deleted"
        else:
            handler = ""
        print("FS: Received change {} of path {}".format(handler, path))
        for oid, observer in enumerate(OBSERVERS):
            observer.get(handler, lambda x: None)(path)


async def read_loop(method, dir):
    if not os.path.exists(dir):
        print("DIR: Local dir {} not existing - creating".format(dir))
        os.makedirs(dir)
    async for change in awatch(dir):
        method(change)


def async_read_loop(loop, *args):
    asyncio.set_event_loop(loop)
    LOOPS.append(loop)
    print('DIR: starting reading async loop...')
    try:
        loop.run_until_complete(read_loop(*args))
    except RuntimeError as e:
        print('DIR: stopping async loop... {}'.format(e))


def parse_created_ts(filename):
    dt = filename.split("_")[0]
    # basic sanity check
    if len(dt) == 8 and dt.isdigit():
        try:
            return datetime.datetime.strptime(dt, '%Y%m%d')
        except ValueError:
            return None
    return None


def file_added(path):
    filename = os.path.basename(path)
    is_archive_dir = os.path.dirname(path).endswith("archived")
    relpath = os.path.relpath(path, BLOG_POSTS_DIR)
    is_media_path = os.path.dirname(relpath).startswith("media/")
    with app.app_context():
        try:
            if is_archive_dir:
                blog_post = BlogPost.get_one_by(filename=filename)
                print("FS: archiving blog post in db with filename {}".format(filename))
                blog_post.archive()
            elif is_media_path:
                return
            else:
                with open(path) as f:
                    print("FS: creating blog post in db with filename {}".format(filename))
                    parsed_created_ts = parse_created_ts(filename)
                    BlogPost.create(filename=filename,
                                    body=f.read(),
                                    created_ts=parsed_created_ts)
        except Exception as e:
            print("ERROR: file added - {}".format(e))
            return


def file_modified(path):
    filename = os.path.basename(path)
    relpath = os.path.relpath(path, BLOG_POSTS_DIR)
    is_media_path = os.path.dirname(relpath).startswith("media/")
    with app.app_context():
        try:
            if is_media_path:
                return
            else:
                blog_post = BlogPost.get_one_by(filename=filename)
                with open(path) as f:
                    print("FS: updating blog post in db with filename {}".format(filename))
                    blog_post.update(body=f.read())
        except Exception as e:
            print("ERROR: file modified - {}".format(e))
            return


def file_deleted(path):
    filename = os.path.basename(path)
    relpath = os.path.relpath(path, BLOG_POSTS_DIR)
    is_media_path = os.path.dirname(relpath).startswith("media/")
    is_archive_dir = os.path.dirname(path).endswith("archived")
    with app.app_context():
        try:
            blog_post = BlogPost.get_one_by(filename=filename)
            if is_archive_dir:
                print("FS: destroying blog post in db with filename {}".format(filename))
                blog_post.destroy()
            elif is_media_path:
                return
            else:
                print("FS: archiving blog post in db with filename {}".format(filename))
                blog_post.archive()
        except Exception as e:
            print("ERROR: file modified - {}".format(e))
            return


def sync_db_dir():
    # get all filenames in dir that are numbers
    blog_posts_in_dir = [f for f in os.listdir(BLOG_POSTS_DIR) if
                         os.path.isfile(os.path.join(BLOG_POSTS_DIR, f)) and not f.startswith('.')]
    with app.app_context():
        blog_posts = BlogPost.get_all_by()
        # sync DB -> filesystem
        for blog_post in blog_posts:
            blog_post_filename = blog_post.filename or str(blog_post.id)
            filepath = os.path.join(BLOG_POSTS_DIR, blog_post_filename)
            if blog_post_filename not in blog_posts_in_dir:
                # file does not exist
                with open(filepath, 'w+') as new_f:
                    print("FS: writing new file blog post filepath {}".format(filepath))
                    new_f.write(blog_post.body)
            else:
                # file exists
                # remove item from filelist:
                blog_posts_in_dir.remove(blog_post_filename)
                # which is newer?
                file_mtime = datetime.datetime.utcfromtimestamp(os.path.getmtime(filepath))
                db_mtime = blog_post.modified_ts or blog_post.created_ts
                print("FILE: {} - mtime: {} - dbmod: {}".format(os.path.basename(filepath), file_mtime, db_mtime))
                with open(filepath) as existing_file:
                    file_body = existing_file.read()
                db_body = blog_post.body
                # check for equality of bodies first, then maybe update
                if file_body == db_body:
                    pass
                elif file_mtime > db_mtime:
                    print("FS: updating blog post in db with filepath {}".format(filepath))
                    blog_post.update(body=file_body)
                else:  # db_mtime > file_mtime
                    with open(filepath, 'w') as existing_file:
                        print("FS: updating blog post in file system with filepath {}".format(filepath))
                        existing_file.write(db_body)
        # if anything is left in the dir, it must be a new blog post from the dir
        for filename in blog_posts_in_dir:
            file_added(os.path.join(BLOG_POSTS_DIR, filename))


def main():
    os.makedirs(BLOG_POSTS_DIR, exist_ok=True)
    sync_db_dir()

    register_observer({
        "file_added": file_added,
        "file_modified": file_modified,
        "file_deleted": file_deleted,
    })
    # start the filewatcher
    loop = asyncio.new_event_loop()
    async_read_loop(loop, notify_observers, BLOG_POSTS_DIR)


if __name__ == '__main__':
    main()
