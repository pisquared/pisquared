# PiSquared Website

This is the repository for [PiSquared website](https://pisquared.xyz)

It's a very simple blogging platform based on [flask](http://flask.pocoo.org/) which has the following features:

* Write / edit blog posts in Markdown format using [simplemde-markdown-editor](https://github.com/sparksuite/simplemde-markdown-editor).
* Archive / delete blog posts and view archived items.
* Search using [flask-whooshee](http://flask-whooshee.readthedocs.io/en/latest/) [TODO: highlighting of search results doesn't work due to xss safety, need a workaround]

## Install locally
Run `INSTALL.sh`.

## Production provision
Production scripts are in `/provision` folder and are fairly generic but were written with simple hosting services like [digitalocean](https://www.digitalocean.com/).

To install on production:

```bash
cp prod_config.sh.sample prod_config.sh
# DO: modify config

cp sensitive.py.sample sensitive.py
# DO: modify secrets

./create_prod.sh
```

## TODO:
* Open index's current page
* Pagination of blog posts
* Tags and filter posts by tag
* Search
* RSS feed
* Comments (?)
* Handle archive / unarchive, rename of files
