import os

basepath = os.path.dirname(os.path.realpath(__file__))
DATABASE_DIR = os.path.join(basepath, "data")
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(DATABASE_DIR, "db.sqlite"))
BLOG_POSTS_DIR = os.path.join(basepath, 'data', 'blog_posts')
