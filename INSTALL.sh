#!/usr/bin/env bash
echo "Provisioning sensitive file"
echo "======================================="
cp sensitive.py.sample sensitive.py

echo "Installing virtualenv"
echo "======================================="
sudo apt update
sudo apt install -y python-virtualenv

echo "Creating virtualenv"
echo "======================================="
virtualenv -p python3 venv
source venv/bin/activate

echo "Installing python packages"
echo "======================================="
pip install -r requirements.txt
pip install --ignore-installed setuptools==36.5.0  # fixes crash with babel

echo "Patching flask_markdown"
echo "======================================="
patch venv/lib/python3.5/site-packages/flaskext/markdown.py provision/markdown.patch
patch venv/lib/python3.5/site-packages/flask_cache/jinja2ext.py provision/flask_cache_jinja.patch
patch venv/lib/python3.5/site-packages/flask_sqlalchemy_cache/core.py provision/flask_sql_cache_core.patch

echo "Init db"
echo "======================================="
export FLASK_APP="webapp.py"

echo "Populating db"
echo "======================================="
source venv/bin/activate && python populate.py
