#!/usr/bin/env bash
echo "REMOTE: Cloning repo"
echo "======================================="
eval `ssh-agent`
ssh-add ~/.ssh/${SKEY}
ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
if [ "$(ls -A ${PROJECT_DIR})" ]; then
    # directory not empty
    cd ${PROJECT_DIR}
    git pull origin master
else
    git clone git@gitlab.com:pisquared/${PROJECT}.git ${PROJECT_DIR}
fi

cd ${PROJECT_DIR}
cp ~/sensitive.py .

echo "REMOTE: Creating virtualenv"
echo "======================================="
virtualenv -p python3 venv
source venv/bin/activate

echo "REMOTE: Installing python packages"
echo "======================================="
pip install -r requirements.txt
pip install --ignore-installed setuptools==36.5.0  # fixes crash with babel

echo "REMOTE: Patching flask_markdown"
echo "======================================="
patch venv/lib/python3.5/site-packages/flaskext/markdown.py provision/markdown.patch
patch venv/lib/python3.5/site-packages/flask_cache/jinja2ext.py provision/flask_cache_jinja.patch
patch venv/lib/python3.5/site-packages/flask_sqlalchemy_cache/core.py provision/flask_sql_cache_core.patch

echo "REMOTE: updgrade db"
echo "======================================="
export FLASK_WEBAPP_ENV="prod"
export FLASK_APP="webapp.py"
python populate.py

sed -e "s|User=.*|User=${PROJECT_USER}|g" provision/systemd.service.sample > provision/systemd.service
sed -i -e "s|WorkingDirectory=.*|WorkingDirectory=${PROJECT_DIR}|g" provision/systemd.service

sed -e "s|User=.*|User=${PROJECT_USER}|g" provision/systemd.service.sample > provision/flatfile_syncer.service
sed -i -e "s|WorkingDirectory=.*|WorkingDirectory=${PROJECT_DIR}|g" provision/flatfile_syncer.service

sed -e "s|example.com|${PROJECT_DOMAIN}|g" provision/nginx.conf.sample > provision/nginx.conf

exit 0
