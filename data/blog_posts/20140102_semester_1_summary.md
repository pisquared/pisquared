# Semester 1 Summary

## Contribution and Achievements

This is not the first time that I worked in a team but it was the first time that the team wasn’t chosen by me but selected on random. So here is to what we’ve done during semester 1:

* **Create a team organization document.** This was our first task as a team but we didn’t have much of an idea what organization should we describe - organization for the Team Project or the PSD assignments? I don’t think this was clearly understood. However we liked the agile methods so we tried to brainstorm about that.

* **Assign risks.** Again, not clear in the beginning. We had no idea about the group assignments and what will this involve. And on the Team Project we were just getting started to understand the scope of the problem - too early to assign risks. So we tried to generalize them and later edit them and narrow them down as things became more clear (and after we got feedback) which was good.

* **Setup trac server and git repository.** I think that was clearly described and we were able to set it up with no trouble.

* **Group work.** We created the two prototypes of programs, trying to keep up with the principles of good software engineering (modularity, abstraction, incremental development) and of the Agile manifesto.

* **Team project.** Our team won the award for the best team project progress. We worked hard, went through several design and architecture iterations, always working close to our manager, receiving constant feedback. Moreover, I believe that we truly want our project to succeed as we see it as something more than just a University task - we actually see the potential in the real world and I think that motivates us.

Despite the difference in experience of the team, we were able to balance the workload and give non-technical tasks to less technically experienced members of the team. I think my greatest personal achievement is patiently understanding the needs of the individuals and yet push them when the deadlines are soon and otherwise the tasks would be doomed. I believe this is a personal achievement since my character is usually much more temperament and I always try to take as many tasks as possible, hardly trusting other people. I think this semester taught me that given chance to people, they will do their best and providing constant feedback would stimulate them to work harder and thus gaining better results.

## Problems Encountered

I wanted to give equal chance of tasting the leadership position to each of the individuals, however in the current form of the team there needs to be an experienced leader like myself in order for work to be done in time. I will try to take this lesson to the second semester.

In other words, the team experience outside of the classroom is various - some had summer internships, worked for companies part time, others didn’t have that option. In such a team if there is no hierarchy, as my experience during the first semester shows, work is done primarily by the more experienced members, widening the gap between the individuals and thus creating a typical Catch-22 scenario. What my plan for the second semester is that rather than concentrating on perfect tasks, we can concentrate on improving team stability and narrowing the gap between the knowledge and experience between the individuals. We can practice agile techniques like pair programming and code review. By producing slightly inferior code in the beginning and investing some time in initial bootup, I believe we can produce an overall better product at the end rather than huge deltas between the individuals of the team.

## Learning

During the semester our regular PSD courses and homework assignments have thought me a great deal of how small and medium companies go through the process of defining the needs, capturing the requirements, making a plan and creating throw-away prototypes.

We learned about different design models, some historical like the waterfall, V and spiral and the more recent ones like agile development. I particularly like the agile techniques - they feel less “business and management” and more “computer science”. I have participated in three hackathons only this semester and created one myself to see that there is a specific culture among the IT guys. We like to do things quickly, play with it, throw it away and start from scratch if it doesn’t work. We are much more like children exploring the ways toys work than grownups who need exact plans and requirements. We can make things quickly and show it to our customer in a week rather than closing ourselves in months or years doing something and in the end failing to deliver whatever the initial requirements were. That is what agile is all about.

Of course, the rest of the world doesn’t always work like that. Therefore we find a balance between what we love and what our customers need. Hackathons and hackathon-like-coding is surely fun but rarely produces code ready for production, since there is no time to write comments, documentation and many times even the code itself is “hacky”. Relatively longer hakcathon-like coding in a week or two is a good compromise between fast coding and good coding. Thus comes the scrum method, identified by its daily standups and status reports and a fictitious leader who is also a developer like the rest of the team but just puts the hat of a leader in the meeting in order to facilitate the meeting.

Of course, we can’t start coding without knowing what we need to do. Here comes capturing of requirements. We had mock interviews with our professors in the University, a short, interview like tuning of the initial requirements. Additionally for our team project, we were having constant monitoring from PhD students and weekly meetings with our manager. The PhD students helped us define what is possible, viable and feasible to do given very high level specifications, iterating through different architectures while the manager started “lowering” the helicopter view he first gave us thus exposing us to more and more details. We are keeping documents in Google docs in order to always be up to date with the requirements as well as specific issues on github as to what precisely needs to be done.

Personally, I am satisfied with the course so far. We had a good amount of practical work, great 25/25/50 division between individual, group work and theoretical knowledge which I would love to see in other courses as well. Learning about theory and applying it in the same week is invaluable learning technique which lasts to years and not just for the exam. Looking forward to the second semester!
