# Week NGC 6402

The consortium was a very interesting experience. 14 people who have hardly ever talked to each other, gathered in a room to discuss something we hadn’t had time to read or understand what exactly is required from us. 13 people kept silent some minutes. I started to speak.

“So... any ideas what to do? Anyone?”

Laugh from 12 people.

“Yeah, well, we were given this piece of paper. And I guess we need to do it.”

Good - I thought to myself. No one takes this seriously.

“Okay, so I see some user stories here. And there is cost and priority. Shall we start reading them one by one?”

11 people blushed and tried to hide as if I was going to pick them. But the others showed a bit of excitement - I guess they thought of me as someone who can lead them and they wanted to just follow. Pure old education - a person (with very, VERY questionable authority) stands in front and speaks and 10 other listen to him.

So we started reading them. And we started voting in the MoSCoW system. That took about half an hour.

“And now what?” - I said. 9 of the fellows never even voiced their opinions.

Silence. Then laugh. We didn’t have the slightest idea. I saw cost. Made a stupid joke about assigning bitcoins. Laugh. Silence.

Then one of the supervisors entered the room. She asked us if we have a plan.

Hah! A plan. Yeah, we had a plan. To lose another day doing PSD. Rather than doing an interesting team project, or read the Operating systems book and playing with Linux. Rather than writing code for creating a web server, making a website, rather than optimising SQL queries and actually doing computing science, we were planning. Oh my Random, I so hate planning!

Another 8 minutes we did mostly nothing while we suggested playing poker or something to assign cost. Someone was running for paper, then we decided to use the board and voice our opinions.

I started to understand what we needed to do. Another MyCampus. AGAIN! In the first semester we did it twice, then threw it away. Now we are going to do it for 7 weeks. And throw it away.

“Shall we move number 6 to the Could section?” someone proposed.

Oh, the heat of the debate! Everyone took part, we started discussing it as if it was our life.

Just kidding. We moved it. Period.

Later in the afternoon, after I was pondering and writing code for my team project (which I’m super psyched about!) we had to return to the room and create components diagram. 5 minutes no one had any idea what is that. I stood up and started drawing. People thought I had an idea. Oh boy!

Anyway, after they got it that I’m just a drawer, not a professor, they started taking part.

![I have no idea what I'm doing dog meme](/media/20140207_week_ngc_6402/i_have_no_idea_what_im_doing.jpg)

It was painful, boring and useless. I thought that we have to separate the components and each team work on a component for the next few weeks, then we gather again and we combine or whatever. Nope. Turns out each team will do it’s job, this meeting was just educational. Got it. Okay. I am educated now, can I not do any more PSD?

Oh, oh! 4 minutes left! We need to draw officially, send the picture and present.

Me and my team mate who actually does work besides me met our supervisor on the way out. The 3 of us spoke briefly about our project. Turns out we are in a pretty good position of publishing a paper! Yey! Enough fun, back to reality...

All righty, presentation. The room was full (unusual). We were the 2nd to go. Each and every one of us were making jokes. No one took it seriously. It was actually good one hour, full of laugh!

What a way to lose a very precious day of 100+ bright young minds! Bravo, University of Glasgow! Bravo! I care about the accreditation from IET, BCS, GTFOFHfKLSHJ78 and all the rest. I would like to please waste another 1 year doing PSI, another countless hours writing blogs and essays.
