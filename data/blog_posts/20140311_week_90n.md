# Week 90n

Formal Methods in Software Engineering. From what I got from the lecture, this is a mathematical way to describe requirements and the process of developing software. 

Here are some links that completely taught me about formal methods:

* [http://homepage.cs.uiowa.edu/~fleck/181.html](http://homepage.cs.uiowa.edu/~fleck/181.html)
* [http://web.mit.edu/16.35/www/lecturenotes/FormalMethods.pdf](http://web.mit.edu/16.35/www/lecturenotes/FormalMethods.pdf)

Of course, that's next to the brilliant lecture slides and note materials. [TODO: Pile of bricks]

This can be useful in safety critical systems and security systems. We were introduced to the idea of the Object Constraint Language. I was left with the impression that the proves can be so hard and tricky, generated from a computer, that it's hard to understand it as a human. However software tools are really not mature enough to work with. Therefore it's a good idea, but without much practical applications.

Other good ideas without much practical applications:

* [Windows 2000](http://www.computerhope.com/jargon/w/win2000.htm)
* Eating a cactus
* [Building a death star](https://petitions.whitehouse.gov/response/isnt-petition-response-youre-looking)
* [Building a death star-building](http://www.independent.co.uk/news/uk/home-news/walkie-talkie-building-in-london-to-have-death-ray-problem-fixed-9127265.html)
* clippy

![clippit](/media/20140311_week_90n/clippit)

Did we pass the phase where we were learning useless stuff for some practical application (like learning how to be semi-interested in a manager meeting) and went into the learning useless stuff for no practical reason?

How I would do things differently is spend some energy into teaching us how to become our own bosses. The idea goes like this: if banks (for example) wants us to know these stuff but everyone is complaining, then get rid of the banks and you'll get rid of complaining students. Just saying.
