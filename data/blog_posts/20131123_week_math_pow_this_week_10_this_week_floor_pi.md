# Week Math.pow((this.Week\ 10), $(this.Week\ floor(pi)))

Quality assurance. Good. Now that we’ve done implementing in our first sprint, it’s good for fresh eyes to have a look at the code. We were pleased to hear the other group liked the code and the design decisions we had while implementing. We also received a great feedback as to what can be improved. For example - we will shorten the commands so that the user doesn’t need to type the whole long string but just one letter. We got several ideas in places where we got stuck. We didn’t discover any bugs that we didn’t already know about but we did mark them clearly in our trac system so that we can squash the ones we know about.

As to the lecture - standardization is a great idea. So great that not one but many organizations exist that do standardizations in different areas in our life. Sometimes however I am afraid this thing happens as noted by Mr. Randal Munroe (xkcd 927 Standards):

![xkcd competing standards](/media/20131123_week_math_pow_this_week_10_this_week_floor_pi/01_standards.png)

Why does this happen? Why can’t we choose just a single way to do something? Actually, I notice lately that if there is even the possibility of doing something in more than one way, no matter how logical and ubiquitous the first one is - someone, somewhere will be the stubborn child of the world that will do the things differently. And will standardize it, and will preach it and teach it and will use it no matter what. Examples?

Dates...

![xkcd iso 8601](/media/20131123_week_math_pow_this_week_10_this_week_floor_pi/02_iso_8601.png)

... Daylight Saving Time (look at 5:08) ...

{% rawhtml %}<iframe width="560" height="315" src="https://www.youtube.com/embed/84aWtseb2-4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>{% endrawhtml %}

... Driving on the left/right (2 possibilities, yet no agreement)...

![map of the world - driving on the left vs driving on the right](/media/20131123_week_math_pow_this_week_10_this_week_floor_pi/03_Countries_driving_on_the_left_or_right.svg)

... the Universal Serial Bus... **UNIVERSAL!!!**

![usb connectors types](/media/20131123_week_math_pow_this_week_10_this_week_floor_pi/04_Usb_connectors.JPG)

... Measurements...
![map of the world - different measurements](/media/20131123_week_math_pow_this_week_10_this_week_floor_pi/05_measurements)

Let me not even start on languages...

