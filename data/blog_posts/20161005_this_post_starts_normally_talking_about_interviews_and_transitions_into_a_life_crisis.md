# This post starts normally talking about interviews and transitions into a life crisis

## Job Interviews
I will be having interviews these days. I guess I am giving up my dream of starting a startup, maybe few years down the road when I get more experience. Don’t judge me. It’s hard. So let’s do some standardized interviews, that is good and stable. Get a job that you will be happy. 

 Algorithms and data structures. Binary search trees, DFS, BFS, Heaps - the regular stuff. You know, the stuff that you do every day as a programmer. Not debugging or refactoring, not testing or googling. A hackerrank ™ online test which shows your capabilities best, implementing things you haven’t done in years but hey - ADS must be important. I can’t possibly have 3 good programmers around me at the time of the interview that google like crazy and help me to pass it.

 If I was the only one complaining, I would consider myself just plain uncapable of my profession. And maybe I am. Maybe the actual coding stars out there don’t complain on HN and can just breeze the interviews about ADS and laugh at the amateurs like me that they don’t know the simple things. Maybe I am so low in the hierarchy that I still can’t see the use, I am just a regular work-man level.

 And that makes sense - I am just 24, no industry experience except my (failing?) startup, a Bachelor degree and an internship or two. I do not pretend my knowledge is good enough. Hell, what do I know? Seriously, no irony, I get it - maybe ADS is the right way. I just highly suspect it is not. /modesty. Let’s move on.

 But if that is true, then why do companies examine me on “tech star” level things? Shouldn’t they just know if I can bash my way out of a stuck python terminal and hire me anyway? If I am supposed to be a low-level worker, why not just test me if I can do for loops, if statements and hire me?

## Company’s incentives
Hypothesis: “A company wants their employees to be obedient.” 

 Hiring is costly. People quitting is costly. People who are adventurous, hackers, people who do open source or side projects, startups are kind of risky. If they hire such a person, she could resign in 6 months because of her side projects. Considering an average 3 months to get up to speed in a company, that leaves (at best) 3 months of work of that candidate. This is not good. And as far I understand, it is rather hard to fire a person these days. So if you hire somebody, it better be for good and better be for long.

 What if we can filter people who we know they will stay for a while longer and will not be tempted to leave? Give them a test that:
A. they will have to spend a week or two brushing up on their skills OR
B. they are so fresh out of college that they will know these stuff anyway and would not have the experience of the real world to tempt them so quickly to leave

 Also ADS is great sweet spot to see if you are/were a good student. And a good student means obedient student - one who does all the exercises and, maybe complains a bit, but excels at them. Why are ADS sweet spot? Well, they are a bit trickier than the other stuff. They do require a bit more abstract thinking, maths etc. “Great” - HRs say - “this is what programmers do, right? Maths? And abstract thinking? Yeah, let’s do that!”

 So now, I am brushing up on my AVL rotation skills. Which I haven’t done in 3 years. And which if I have to do anytime after the interview, I will just google and copy the code anyway (of course, only from SO or github with the proper licence and attributions, yadda-yadda). But I wouldn’t trust myself to do it on the spot in 15 minutes. Why not? See, I would trust myself setting up a flask webserver on the spot even with no googling. Or writing routes, models, doing migrations, scheduling tasks with workers, running a debugger. I will trust myself doing list comprehension, designing classes and inheritance, refactoring or unit testing, I would trust myself using bash to find files that contain a string recursively, changing permissions, setting up nginx, uwsgi etc. Why? Because I’ve done this hundreds of times and the first 20 or so with googling. Then it becomes quicker to get it from brain than the slow Internet, we all know I/O kills everything, brain is quicker. But my small cache doesn’t really have AVL because it wasn’t accessed that often.

## Why is that possibly true
I can’t prove my hypothesis that companies do that only because obedient people would sit down and study the things that a company tells them to do. I mean, it makes sense I think, but again, I have very limited experience and maybe once you grow up from setting webservers, you do start writing algorithms. Maybe having algorithms in cache is a good signal or correlates well with “knows how to do shit”. If there is research like that, please let me know. 

 And don’t get me wrong, I can reason why to use a list or a dict at certain situation, I do think about my data structures, I do think about algorithms. But I don’t write my own merge sort, ever, I use sorted(). I don’t write my hash functions with random probing collision resolution, although I was interested enough to look up how python does it and am aware of edge cases and limitations - but even that is trivia that I don’t really need to know. Just I need to know dictionaries are better for looking up keys than storing the same info in a list of lists. And other things of course.

 But I think it’s correct, the hypothesis I mean, because I’ve heard and read about tons of programmers who I consider much, much better than me to complain about the same thing. Complain that they couldn’t pass a Google interview because they asked them to implement a red-black tree on the spot in 15 minutes and they couldn’t. But if they could it would mean that they have a track record of knowing how to do it in the past or are scared enough to sit down and learn that thing for a company because they need a job. Or that they are pure machines that eat Algs for breakfast, Data structs for lunch and dynamically program their pizza for dinner.

 So what is a good interview then?
Billion dollar question isn’t it. Seems that nobody knows, or at least that is what I read. Bad things that we know didn’t work in the past:

 - **Brain teasers** - You know, you have 10 coins, 3 of them lie all the time the rest are put inside of a blender that starts spinning in 10 seconds, what would happen if you put the blender on a manhole which is square and how would you time these 10 seconds given you only have a boat that can carry only one thing and a giraffe inside of a fridge? These turn out not to be good predictors for success, although they were asked by the Big 5 in the past. Most Good companies ® stopped using them. I used to play tons of this in the time when Internet was still not a thing and I was socializing with the children at my grandma’s place. My 5 year old-self would be able to pass these interviews, no problems, I knew so many words, in fact I knew the best words, I had the best words... but not anymore.
- **Trivia** - what is the difference between finalize, final, finesse, what ECT3-omicron stands for in AWS, and what if you type []+[] in a javascript console. Similar to brainteasers, you are probing your luck with those - the candidate either knows them or doesn’t and there is no much thinking or explanation that can be given. A good Who wants to be a millionaire programmer question for $25,000 but with no 4 possible answers and can’t call a friend. Brain teasers are probably actually better than these, since the candidate can at least show some logical thinking. 
- **Maths**. Duh. Answering 100 questions in 25 minutes aptitude-get interview that a calculator would do and if my fingers are fast is not a good predictor except maybe that I am a fast typer and have drunk 2 red bulls before that. And if you give me some of that crazy alien symbols, some of which are greek, some of which are inverted greek that don’t even look like an equation but behave like one if you divide both sides by square root of -1… I will just give you a reference to a friend of mine who gets high on square roots.
- **IQ**. Maybe a good predictor still but as with all tests, if I practice enough pattern seeking and number sequences, I am pretty sure I can get 30 points which is a lot, right?

 I think ADS will be added to this list at some point in time. It just sounds too much like “if I study it for a week or two, I can fake it for 20 minutes”, similar to the above of the list.

 So what is left?
 
**Writing actual code**. Can’t go wrong with that and many companies do it. If I have to hire a plumber and he can’t do pipes, I don’t care too much if his engineering skills are a blast and he could draw me a sketch of the Golden gate bridge in a flash.

 But then how should candidates write code? IDE, text editor or white board? Can they use Google or documentation or man pages? Only brain cache? What is the measurement? Time? Space? LOC? Yeah, I think that last one would be great. It’s so easy to pass it to an HR and measure the amount of LOC - the more lines, the better programmer and it’s an easy decision! HIRE!

 These are hard questions and it probably depends on a company. You are free to decide, of course. But you will be wrong if you don’t decide like me. MY BLOG POST, MY RULES, GET OUT OF HERE!

 I would go with full blast - you can use IDE or vim, you can use docs and duckduckgo or whatever you will use if this actual problem is given to you at work. Hell, why limit her? I don’t get it - let her call a friend, ask around, ask the interviewer, try things around, run it, break it, let her watch a youtube video of a cat or whatever she is into (no porn… unless it’s… no, no porn), stretch a bit, go for coffee, come back, try again… Why limit the available resources when programming is hard enough even when you do have all the resources?! I mean, if it was that easy to code with Google, probably I would be a superstar coder immediately, no? 

 I would give only 2 restrictions or differences:
 
- You can’t copy-paste code verbatim. In any way it is a good practice to remember code and avoids liability for a company, especially if the code is not explicitly stated as free to use
- You have to sort-of walk us through what you are doing and why you are doing it. Which is not that much different than pair programming.

 And you can BYOLaptop if you want, show us some cool .vimrc or whatever to kick things off, make it more of a normal conversation. Of course, there is an imbalance of nervousness on the sides of the interview and it’s hard to balance this one, the candidate has a lot to lose but if I can do this on an interview, I would be 10 times calmer since it almost sounds like a normal work day. 

 And for fucks sake, don’t ask me about my weaknesses if you don’t want to hear them.

**SPOILER: The crazy part starts here. Mom, please don't read below this.**

 My weaknesses? I am an angry, lonely, insecure perfectionist who gets distracted by sexy red heads, solving his problems with occasional running or eating until regret, over-stressed quarter-life crisis millennial who believes is entitled to a meaningful job, an over-controlling freak that draws diagrams at 2 o’clock in the morning of the-meaning-of-my-life, fucks that, then fucks his right hand instead of his girlfriend because he believes his life is worthless and he doesn’t deserve to be happy whatever that means. But I think that improves me in a way, yeah? How are you crazy? What do you do at 2 o’clock in the middle of the night? Do you check your facebook wall? Or do you think about work? I know I think about my startup and how to make it work even though I have said about 50 times in the past month that I am done and only 45 times that I think it will actually work, is that crazy? I know I dumb my senses with binge-watching Mr Robot because I don’t want to face real-life decisions. How do you destress Mrs HR? Are you happy, do you stay positive?

 Don’t ask me about my 5-year plan. I am not in school anymore, yeah? I don’t know what 5 years is anymore, too many decisions. Will I have a baby? Will Trump become president and start a nuclear war? Would I do programming or switch to something else because of FOMO? How should I know? Do you? Can you predict the future? Can you give me your plan? Can I follow your plan? Because you have one, and I don’t, yours is better? Let’s just build a wall.

 Will I be a team player or a leader? Better both. I can show you examples of these, does it matter? Am I hired? Am I a good person?

 Oh, boy, I am complaining. I am a white young male in the western world with first world problems, shit! I have too many choices - I can become anything! ANYTHING, you hear?! ANYTHING! People in Africa would kill for these choices. And I sit here complaining when I should be studying rotations. Fuck!

 I don’t want choices. I am too smart. I am too dumb. My parents know what I should do. My girlfriend knows what I should do. My business partner knows what I should do. How come I don’t know what to do?

 I don’t know. I ain’t no HR. Obviously, for now I ain’t no businessman either. I will probably try again at some point. Now, I need to pay the rent. Or should I go back to my home? No, that’s not good, we decided we are a “person-who-lives-in-the-western-world”. We? I mean I. I MEAN ME. Suck up with interviews, give up my entitlements, dumb my senses, starts living an average life. “Just” be normal. “Just” stay positive. “Just” enjoy the journey, the beautiful life you’ve been given. You have 1 in a trillion chance being born and you just have that many years to enjoy the journey. It’s a struggle, everybody struggles, but you learn from it. Hapiness must come from within, you can't control the external stuff. You are responsible for your own life and you decide how to me happy. Here, read that self-help book. Here, watch that guru talking on youtube, he has his life figured out. 

 Meet friends, don’t isolate yourself.

 But I hate people.

 But I am lonely.

 I need people.

 But I hate people. I should hate people right? I am an introvert.

 But I love my girlfriend. Yeah, let’s just stick with that. Make a family. Enjoy the little things.

 But I need to socialize. 

 Let’s go to a party.

 Fuck, I don’t want to talk about “what do I do”? I don’t know what do I do. What do you mean, I guess I code? I sort-of-have a business. I don't anymore. Or I do? How do you decide if your business fails? It's a mirage. But it's my baby. Would I ever give up on my baby? Should I give up.

 Dude, just chill. You are living the dream. Just enjoy it, it’s not that bad, you have friends, family, you are healthy, you are smart, educated. Why do you think you have so many problems? You know how many problems I have? Dude, tons of problems, yours are nothing! You have a choice, you can work wherever you want. Just send your CV, just pass that interview at that company. Just be happy, okay?

 “Just” say “just” one more time motherfucker, say “just” one more time, I dare you, I double dare you!
