# Facebook (and the rest) - wait a minute - this way is wrong! 

This is not the way we wanted computers to work. We don't want people addicted to their phones all the time, looking like zombies, internally feeling depressed and anxious and always trying to outcompete everybody in the world; trying to show off to get a few more likes, to get a few more comments. We don't want this poisonous competition of individuals for meaningless virtual points. And all the while these same people are being exploited by Facebook to click on things so that advertisers can show them ads to things they probably don't even want. We don't want people reading shallow automatically generated articles of confirmation bias, which are being promoted by Facebook to get even more clicks for more ads for more things you don't want.

We wanted to connect with the people we care about. It's our intrinsic desire in this more and more isolated society. We wanted to know what is going on in the lives of the people we cared about but not to compete with them in this ugly jelaous way. When I see a real friend of mine going on vacation - I like it, I feel good he feels good. But when I see it in a person I barely know - I see it as a showoff.

What we got is connection with companies. We got hundreds of messaging apps, photo apps and paradoxically - it's making it harder to connect with people. People use something different for different reasons and it's impossible to message inbetween apps like it is possible to send SMS to people on different mobile networks. 

Facebook seems like a behemot, seems like this huge company that is here to stay. It has done wrong many times, it has changed its algorithms to promote biased content to keep you with interacting with the site, it has created walled gardens, it is ruled by an asshole who has said privacy is not expected but he keeps a tape on his camera. It had broken promises and apologized and broke promises again. It has Instagram and Whatsapp and many people don't even know it - so if you want to escape, it is there to capture you again.

It is not how we wanted our society to work. I don't think Steve Jobs wanted this when he and the rest of Apple envisioned the smart phone. I don't think he would be happy to see Apple has worked a few years with it's best engineers on hand to create animated emojis. It is not how we wanted our computing to work.

We want to be able to learn new skills, to create things and express ourselves, to connect with people and keep the important ones close to us. That's what fundamentally technology should be about.










