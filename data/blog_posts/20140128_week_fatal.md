# Week Fatal

Architectural design patterns brought back memories from last year’s software design patterns in OOSE2. Even though most of them were known to me, it was good refresher and an easy topic to read about.

I remember that last year I finally got to know the model-view-controller (MVC) after practicing it in one of the other courses assessed exercises. Seems that these patterns are almost everywhere (duh) as we are discussing some of them (client-server, peer-to-peer) also this year in DIM3 and even OS3 (pipe and filter, message oriented).

While I was studying for the finals last year, I remember I found this guy on youtube who turns out is making very, very useful videos about many topics in computing science. Here is the playlist with his design patterns (code is also provided and thoughtfully commented):

{% rawhtml %}<iframe width="560" height="315" src="https://www.youtube.com/embed/vNHpsC5ng_E?list=PLF206E906175C7E07" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>{% endrawhtml %}
