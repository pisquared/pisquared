# Week Ans^wer

As far as refractoring goes, here are some links that I find really useful:

* [http://refactoring.com/](http://refactoring.com/)
* [http://msdn.microsoft.com/en-us/library/719exd8s.aspx](http://msdn.microsoft.com/en-us/library/719exd8s.aspx)
* [http://www.c2.com/cgi/wiki?WhatIsRefactoring](http://www.c2.com/cgi/wiki?WhatIsRefactoring)

Of course, I have used the refractoring facilities of Eclipse, I learnt that during the Google's Android camp in 2012 which was structured in a hackatonish way. I find it really useful as it saves time and helps me write better code. 

![Dogge meme - Such code, very IT. Much Edit](/media/20140219_week_answer/dogge.png)

As far as software comprehension goes, I have had some experience during the summer reading other's people code. I used profilers as well. It helps to understand RAM and CPU in much deeper level. I actually asked them once to go to dinner. They refused. They said things are moving too quickly. Here are some links that I find useful:

* [http://grok2.tripod.com/code_comprehension.html](http://grok2.tripod.com/code_comprehension.html)
* [http://www.developerdotstar.com/mag/articles/read_codecomp.html](http://www.developerdotstar.com/mag/articles/read_codecomp.html)
* [http://www.c2.com/cgi/wiki?TipsForReadingCode](http://www.c2.com/cgi/wiki?TipsForReadingCode)

I would *praise* the material in the lectures. I think it was useful for people who don't know these things. However, I would *critisize* that we are learning that in third year - if people didn't know that already, they've been doing things very wrong with eclipse (basically using it as a text editor, not IDE). If they did know it like me, well, then it was useless.

*How I would tackle it differently* is explained in the second year when we are doing Java.

As my previous post here was censored(+), I provide a nice little story which summarizes my experience with PSD so far. It is relevant to this blog because it actually taught me something - your oppinion doesn't matter. You cannot change a system in which banks are involved (unless you are richer than a bank) and you cannot tag people.

(+) - Well, actually you can find it here: [The shape of the Universe](https://pisquared.xyz/20140219_the_shape_of_the_universe.md)

## The Dynamics of Ovis Aries
Let me tell you a nice little story that doesn't begin with “once upon a time...” since the spacetime curvature actually bends a bit differently these days and as a result the time is now.

Apparently, in our Universe, there is a big bad wolf who screams at the sheep when they use bad words or in any other way try to talk directly. So now, I am going to talk indirectly. I am going to call the things with their fake names.

Anyway, having a protector is good – big bad wolfs are a good thing, otherwise who would the sheep be scared of? So he can scream and take you to even bigger, badder wolves who can talk to you calmly and disciplinary action you.

But the big bad wolf is busy. He is running a whole northern part of the Universe. So instead, he makes the little good wolf to talk to one of the black sheep. This particular black sheep had made an earth shattering discovery several days earlier and natuarally, the Universe needs to protect itself from such discoveries, otherwise it will not be interesting for the generations to come.

In the mean time, some of the pure white sheep have also decided to become greyish, testing the ground, trying to support the black one. The black one is very thankful and loves the sheep that became even a tiny bit grey over the days!

The small good wolf tells the black sheep that the grass is good, there is nothing to complain about. Even though the grass is full of sh..ampoo which makes it soapy, slippery and inadequate to process, the small good wolf says that that's how the grass is in the real world. And when one day we go to the real fields, we will see grass with shampoo everywhere and everyone will require us to eat it. And if the black sheep wants to become at least a small wolf, he needs to trust the ones who are already wolves. “But” - the black sheep thought to himself - “do I want to become a wolf at all? Maybe I want to become a tiger. Or a lion.”. Well, no. This Universe teaches you how to become a wolf. Wolves work at banks and they are really rich and they eat the finest, the Head&Shoulders of soapy grass! And then they advise that the Universe should promote soapy grass. And so we are all connected in the Great Circle of Life...

So the black sheep listens and nods and decides that he doesn't want any sized wolf talking or shouting at him anymore. He has tons of grass to eat, more than he ever even hoped for. Some of the grass is actually really pointy (which simply put, just points to another grass) and hard to process.

In addition, the black sheep promises to work constructivly with sheep who just want to starve and not eat any grass. The black sheep will therefore construct a hut, because he had tried to construct everything else in the past and he ain't no architect to think of anything else.

So in conclusion, the black sheep is really sorry, he will try not to discover anything else and eat the soapy grass in a timely manner.

The end.

{% rawhtml %}<iframe width="560" height="315" src="https://www.youtube.com/embed/ky-U15Hkw4g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>{% endrawhtml %}
