# Summary entry II

If the first semester was balanced and happy with occasional hiccups, the second semester was quite a different story. Courses like Operating Systems, Network Systems and Databases were much more demanding both in theoretical knowledge and assignments. I enjoyed them and now I can say I get these areas much deeper thanks to both the material in the lectures and the assignments which also helped me solidify my knowledge in C. It was interesting (from an academic practice point) to build a web server, chat program and disk driver even though not many of us will have to do this in the future. However, it definitely made me get a better understanding of some of the inner processes. On the other hand, optimising database queries can actually be applied to the real world and I am happy we did that.

In addition to that, the team project had reached a phase beyond just mocks and requirements capture and implementation work needed to be done.

Another semester long project was the Distributed Information Management assignment which required building a website from scratch using Django web development framework. This was not interesting or valuable for me as I already knew how to do all of that and therefore tried to encourage the other people from my team to spend a bit more time on it and I would help filling in whatever parts were unclear. In the end, we had a working application but it was a result of mainly my work and one of the team members didn’t participate at all whatsoever.

## Contribution and Achievements

On the PSD front, unfortunately, I can’t say what stayed in my head except constant frustration with the setting up of  frameworks. As I said several times in this blog, a framework cannot be taught in a week. Getting the “taste” of it clearly didn’t work for me as I couldn’t see the practical application. If PSD was the only subject that I had to concentrate on, surely, I would be able to practice AspectJ, JBehave, Jenkins, OSGi, Apache Felix, Ant and Ivy. However, next to the other subjects, I had maximum of one day in the week to actually try to do that. It didn’t work.

The assignments (bi-weekly sprints) were implemented with the limited understanding that we got from the frameworks. As it was mentioned the more important part had to be team work. So I decided that I shouldn’t concentrate that much on the technicalities but try to separate the workload between the people. And here we get to the second part of this summary.

## Problems Encountered

I suppose I had constant negative criticism of the PSD course, which I hope is obvious in my previous posts, student-staff meeting (in which I tried to summarize the general feeling, not only mine, but the whole class) and in the feedback for the third year. Basically it boils down to 1) Too many frameworks 2) Artificial assignments and 3) Too much assuming that everyone will work for a company, rather than venturing something of our own. Even with all these complaints, that would be mostly irrelevant had the teams been better balanced.

If I could genuinely separate the workload of the course to the other team members in roughly equal parts, the assignments would be much more doable and enjoyable. However what I noticed not only in my team, but in many others is the fact that one or two of the people were doing the majority of the work with the other two doing nothing to very little amount of work. My working hypothesis is that the pseudo-random algorithm of separating people in teams was “two with above average marks + two with below average marks” in order to equalize it. Let’s assume that this was indeed the case.

What this easily leads to is demoralizing the people with lower grades (as they quickly can see that they can’t keep up) while at the same time annoying the people with the higher grades and effectively making them do all the work. This leads to a positive feedback loop in which the described effects are amplified during the year. Additionally people with the higher grades have (usually) always been above the average and thus want to have the good grades at the end of this year as well. Same for the people with the lower grades - they are used to not showing the maximum and thus don’t care about it so much.

Of course the deltas are supposed to solve the problem with the grades. However this doesn’t help much with the total less acquisition of knowledge and practice in the average case as the feeling of doing all the work feels unfair through the year.

If this way of combining people in the team is in fact true, then this needs to be rethought for next years. I would vote for stricter exams at the end of 2nd year and combining people based on roughly equal ability rather than trying to compensate for the slow learners. Slow learners will always be that way, smart learners can’t teach them what they couldn’t acquire in 20 years of their lives!

Lastly, I would like to seriously criticize the argument that “This is how teams work in the real world”. First, people who don’t do work will be fired. I have seen and heard about people who have done absolutely 0 work (no group meetings participations, 0 lines of code for any of the team projects, constant deferring and not taking responsibility for work etc.). These people would be fired in the “real world”. Second, in many tech companies you can get some choice of the people in the team (this is not 1980s!) and if something doesn’t work, there are mechanisms to raise this issue. In the University, there is no such mechanism.

## Learning

The things that I actually found useful in this second part of the course is the components idea and continuous integration idea. The components does provide a good separation if implemented correctly and people can (in theory) work on different parts of the system and then combine it to result in a big, cohesive system. The continuous integration is another thing that I enjoyed - fully automate everything. One of the teammates actually is part-time working and helped me understand and showed me some useful tricks. Unfortunately I didn’t have time to practice these correctly, but rather hack working solutions for the assignments.

Lastly, talking about hacking, I also got to know that nothing of this matters as long as you can talk convincingly and give your ideas to the right people. This can win you a £20,000 hackathon while doing all the other things wrong. It's a great story and if you would like to read more about it, I would recommend reading my real blog.

And finally, out of all models that we learnt this year, I found this one the most useful: 

## [http://programming-!@#$%^&*.com/](http://programming-motherfucker.com/)
