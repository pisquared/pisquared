# Week ceil(pi)

Requirements are better than planning. I know that probably I should embrace both as good strategy for work, but I just can't. I am more the strategy guy than tactics guy. I like to know that in chess the goal is to checkmate the opposite king. This is the requirement. Now how I'm going to do that - using pawns, bishops and knights - I want to have the flexibility to do that. I don't want to make my first move and define my whole game. I like the dynamics, dealing with unforeseen positions.

There is this big difference I see which makes me want to have good requirements but not necessary a strict plan to execute them (see rant about planning, pre-previous blog post). But the specifications are whole different world - I would like to know what is the final deliverable, what needs to be done.

Describing this with the help of the UML is best. One language to rule them all - standartization is good practice and it's of the rare case that UML was created. Drawing the diagrams, writing the use cases shows you a good picture of what the final thing will look like. It's so good that you can draw java class hierarchies and tiny actors and use the same universal approach. Good things.

To keep it rolling from the last week, I rewrote a famous song about the requirements.

## We Will Require

*At first, we were afraid, this was not defined.*

*Kept thinking, we could never write the specs we were assigned.*

*But then we read so many slides thinking how we did it wrong.*

*And we did what we shouldn't have prolonged.*

{% rawhtml %}<br>{% endrawhtml %}

*And now we write, and ask and chase*

*proffessor Singer down the corridor, refining our case.*

*We should writte that good old doc.*

*We should made a guarantee.*

*that this will not impact, oh no, the mark on our degree!*

{% rawhtml %}<br>{% endrawhtml %} 

*Could, would, should, won't - the keywords are.*

*What do we need - we won't ponder anymore.*

*Who though of that? It just didn't ring a bell.*

*Who could know? ... We had to use the UML!*

{% rawhtml %}<br>{% endrawhtml %}

*Oh, no, not we, we will require!*

*Oh, as long as we know how to trac, we'll keep a good attire.*

*We've got all specs to give, we've all got to get relief*

*And we'll require! We will require, hey, hey!*
