# AFTER ENTER - Episode 2 - Pressing!

The story so far:

* [Episode 1 of After Enter - Before the Enter](/20151113_after_enter_episode_1_before_the_enter.md)

Hi again. Last time we spoke about what happens before a search engine like Google has to accomplish before doing a search through billions of documents that make the Internet. Today we are starting our journey by doing a real search.

Our particular search will be the age old question that every child asks and every adult is ashamed to not know the answer:


*Why is the sky blue?*


How many ways can you think of doing that search? If you are on a phone or have a smartwatch you can say “OK Google, (hear a small bleep) why is the sky blue?”. Or you can tap on your home screen box and type with the keyboard that appears on the screen. If you are at home and have a laptop on your… well, lap… you could type on your keyboard.

So which one was it for you? This interaction is called **interfacing**. The word **interface** is used frequently and interchangeably but basically it means what are the ways to communicate from one thing to another. If you used your laptop keyboard, that was an interface. Inter meaning between and face - ummm … face? On the other hand the keyboard interfaced with the computer via some internal cables, the laptop interfaces with the internet via WiFi, cable or mobile network and so on.

Okay so you typed it and pressed enter or said it and are now waiting. Well you don't wait for too long because the response comes almost instantly. But the story is incredible! Listen…

Let's assume you pressed enter and we slow down time a LOT. So much that a blink of an eye will take the next 10 episodes. That's long… 

All right, here we go!

Let’s look around, what is happening. A keyboard consists of electrical switches in a similar way a lamp switch is made. 

![Key schematic](media/20151120_after_enter_episode_2_pressing/02_keyboard_first.png)


Now, see that switch. The two cables have electrons which stay relatively static. (Well, that's a lie, they move all the time buzzing randomly like flies.) Why do they buzz randomly? They are excited but they don't feel strongly to move one way or another. There is no direction in their life. It's like when you are in a big pool and you  have more than enough energy to enjoy swimming but with no particular direction. Where is that energy coming from? The loud Big bang. Now, concentrate!

Once we press the button, closing the switch the electrons start feeling a force. It's like a wind starts blowing in one direction and they slowly obey. They still are kind of randomly moving, but randomly with direction. 

![Key press](media/20151120_after_enter_episode_2_pressing/02_keyboard_press.png)

While our finger is still on the keyboard, the electrons start flowing in one direction. Where is that force actually coming from? Your finger? Not exactly. When you turn on your lamp, your finger just opens the gates for power to go. Now how does your computer know that?

Well, your keyboard is actually not just these two wires connecting the enter. There are many more connecting each key. But if there was one cable connecting each key, that will be a lot of cables! Your keyboard will be big and bulky. 

No, let's not do that. Let's organize the cables in a **matrix**. Now in this case a matrix is a fancy way to say that cables form grids on two layers. If you have an old keyboard connected to a desktop computer, I would disassemble it and see what's inside. It's pretty cool! You’ll see two thin plastic sheets with some very thin paths made of some metal separated by a third layer with holes… it's like a sandwich with emmental cheese, mmm..

![Metal sandwich](media/20151120_after_enter_episode_2_pressing/02_metal_sandwich.png)

So when you press the button you connect the two pieces of bread of this sandwich through the cheese which lets these electrons flow.

Now the keyboard has small computer itself called a **controller**. What it does is checks every thousandth of a second or so which two metal pieces of the two grids made contact if any. If any of them did, the controller records that in a memory of its own called the **endpoint**.

And then it waits. Remember, we slowed down time a lot. The controller checks every thousandth of a second just to be sure but there is no typewriter in the world that is that fast. 

{% rawhtml %}<iframe width="560" height="315" src="https://www.youtube.com/embed/m9EXEpjSDEw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>{% endrawhtml %}

Turns out the the fastest of the fastest types about 200 words per minute which is about 1000 characters per minute equating to merely 15 keystrokes per second. I said merely because from our standpoint that's eternity. In real time, that's pretty impressive!

This is of course in the case of a real, solid keyboard. What if it was one on your smartphone?

![finger pressing smart display](media/20151120_after_enter_episode_2_pressing/02_virtual_keyboard.png)

Well, it’s pretty similar, just your finger acts as a connector between the two components. Don’t worry, these electrons are actually pretty tiny and won’t hurt you.

Anyway, the information about the press got to the keyboard controller. And the keyboard controller waits. For what? Remember we slowed down time. The keyboard controller queries for keypresses thousands of times per second but the computer queries the keyboard much less often. There are two ways computer stuff communicate - they either **push** or they **pull**. Let’s say our keyboard controller waits for a pull from the computer. If it was more pushy type, it would flush down what it gathered down the line for the computer to collect and not wait at all.

Why the two ways?. Well, the brain of any computer, called the **Central Processing Unit** or **CPU** is a very important and busy boss. It allows you to listen to music while you type, move your mouse, browse the Internet and many other tasks.

![Workers queue](media/20151120_after_enter_episode_2_pressing02_queue.png)

Now she can either order that some of her workers go and fetch things in which case the workers go, **pull** and then queue or if the keyboard **pushes** a message, the keyboard worker queues. This queue exists so that you can do many things seemingly at the same time. What actually happens is that the boss reads messages really quickly and passes messages around the workers.


![Fast workers queue](media/20151120_after_enter_episode_2_pressing/02_quick_workers.png) 

It depends on many factors whether the communication will be push or pull. Some interfaces like USB wait for pull from the computer, other push. In the end, the messages always end up in the queue for the boss.

All right. So a worker goes to the keyboard controller endpoint and asks: “Do you have anything new for me?” to which the keyboard controller answers: “Yes, indeed I do. Here is what I gathered in the last 10ms” and passes the code for Enter.

This is very cute and all but it’s not really what happens. See, there are no small humans inside your machine. There are just wires and electrons. What I just pictured above is a model of how it works because you, reader, are probably a human and humans speak abstractions. The real language that the keyboard controller communicates with the computer speak is not really English. It’s called **binary**.

Binary is the language that comprises of two letters - 0 and 1. Zero means no (or very weak) signal, one means (stronger) signal. The controller actually sends these signals in a specific variation in order to **encode** the message that the key was pressed down the wire to the cable connecting via the computer interface.

## Let’s recap: 

You press a button on a physical or virtual keyboard which is the interface to your machine. A physical keyboard has a matrix or a grid of electrical conductors separated by a layer of electrical insulator like a sandwich with emmental cheese. By pressing the button you let some small amount of electrons move around. The keyboard controller reads the matrix about a thousand times a second to check for changes, saving the change in a memory location called the endpoint. Now it can either push this data down the cable to the computer or wait to be pulled. Computers communicate in a language called binary which uses only zeros and ones as letters.

So far we know what has to happen before a search occurs and what happens when you press a button of a keyboard. The information about your query has just started - 10 milliseconds into the trip only your keyboard knows that you have asked something. The journey continues next time as we follow the information flowing into your computer and what happens there.

Stay tuned!
