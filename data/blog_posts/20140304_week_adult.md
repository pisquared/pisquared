# Week Adult (in most European countries)

We did state machine diagrams in year 2, computer systems course. We dealt with finite state machines and explored their properties in semester 1 in Algorithmics 3. (Personal background experience noted) But until now, we had no idea how do draw state machine diagrams.

So now we know! Here are some links to support my thesis:

1. [http://www.dcs.gla.ac.uk/people/personal/gethin/alg3/2013/alg3_section6.pdf](http://www.dcs.gla.ac.uk/people/personal/gethin/alg3/2013/alg3_section6.pdf)

2. [http://www.dcs.gla.ac.uk/people/personal/gethin/alg3/2013/alg3_pushdown_automata.pdf](http://www.dcs.gla.ac.uk/people/personal/gethin/alg3/2013/alg3_pushdown_automata.pdf)

The second part of the lecture - Threads! We did that too in Advanced programming 3, semester 1. With the second assessed excersise being on Java threads. And refreshed it in Networking Systems, semester 2. And the assessed excercise in NS was with C Threads. And the OS assessed excersise requires knowledge of C Threads (with tutorial refreshing the concepts).

But I think it's the first time that we see a UML state diagram.

Here are some links to support what I am saying:

1. [http://moodle2.gla.ac.uk/pluginfile.php/120556/mod_resource/content/1/Concurrency2a.pdf](http://moodle2.gla.ac.uk/pluginfile.php/120556/mod_resource/content/1/Concurrency2a.pdf)

2. [http://moodle2.gla.ac.uk/pluginfile.php/193944/mod_resource/content/1/lab2.pdf](http://moodle2.gla.ac.uk/pluginfile.php/193944/mod_resource/content/1/lab2.pdf)

3. [http://moodle2.gla.ac.uk/pluginfile.php/206062/mod_resource/content/1/Pthreads_tutorial.pdf](http://moodle2.gla.ac.uk/pluginfile.php/206062/mod_resource/content/1/Pthreads_tutorial.pdf)

4. [http://moodle2.gla.ac.uk/pluginfile.php/120575/mod_resource/content/1/General1.pdf](http://moodle2.gla.ac.uk/pluginfile.php/120575/mod_resource/content/1/General1.pdf)

I know the repetition is the mother of learning but I would argue with that. If I repeat a 1000 times the expression "Hello world" to a dog, it will not be able to learn it how to do it himself, although it would react somehow the next time I mention it. The doing of the assessed excersices help in the different courses but introducing us to threads once we have done it for a year... Maybe as well, who am I to judje?

How would I do things differently? Spend some time making the courses more coherent. (A constructive suggestion.)

Appraisal: I liked that the lecture was moved to 10 o'clock. I am surprised how suddenly the room was made available for us one hour later on Monday. Many questions will remain unanswered though: who was occupuing the room at that time all year? What happened to them? Is the University haunted by aliens?

![i'm not saying it's aliens... but it's aliens](/media/20140304_week_adult/aliens.jpg)
