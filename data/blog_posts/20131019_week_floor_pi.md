# Week floor(pi)

I can't be more confused about git. I'm using it for about two years, pretty regularly, I get the point, I know it's good. But somehow I always manage to screw things up.

Every time I discover something new about git, I love it and I think "NOW I got it!". And after few minutes, git slaps me like a lemon tied on a brick (H2G2 ref). I wish I could work better with it. I went through so many tutorials, I worked with it, I asked for help from more proffesional users. And I still manually copy the directory just in case. Version control is good, the idea is great, I love it to be able to undo things, to see how a project evolved with history, to branch and play on a new feature. But does it have to be so complicated? Can't it be ala Google Docs solution? Future will show.

In the meantime - I decided to write a poem about git. It's rephrased very epic poem we have in Bulgaria called "The Rebels on Shipka" and it describes one of the last battles we had with the Ottoman empire back in the end of XIX century. I love it so much, I decided to take a spinoff of it. There it is:

## Ode to Version Control

![battle of shipka](media/20131019_week_floor_pi/shipka.jpg)

*Oh, Version control!*

*Three weeks already the young and the brave*

*the code they manage - and not misbehave!*

*Github valleys eagerly repeat the roar of the battle.*

*Horrible pushes! For the dozen time the old merges rattle,*

*and fastforwards automatic clear some mess.*

*Commits after commits! Pushes after pulls!*

*The scrum master is pointing to the specs*

*and shouts "Branch it first! Perform all checks!"*

*And developers rebase, they talk and then agree.*

*But github answers with another shout: Permission denied (publickey)!*

*And with new rain of checkouts, statuses and logs,*

*everyone blames, and clones, and forks.*

*They code like lions, they compile like sheeps.*

*The last commit had broken the release.*

*And again git wins. The geeks disapointed, frustrated*

*just do in the bash:*

*rm -rf /*

-------------------------------- 

*But the day will come,*

*trust me, I know,*

*where we will understand*

*how to use version control.*
