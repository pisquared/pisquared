# Week Least Random Number

Aspect Oriented development.

I loved it. I think it aspect oriented programming is really useful. I think it creates the abstraction needed for professional software development. I think the day that we spent using AspectJ was absolutely enough to grasp the main point. Maybe not understand it completely, but really get what's going on. 

I didn't have any previous experience with that, so that didn't help me a lot. But I understand the idea. And that's important!

Here are some links which helped me while understanding deeply aspect oriented development - totally not result 1,2,3 of a search engine:

1. [http://en.wikipedia.org/wiki/Aspect-oriented_programming](http://en.wikipedia.org/wiki/Aspect-oriented_programming)

2. [http://www.onjava.com/pub/a/onjava/2004/01/14/aop.html](http://www.onjava.com/pub/a/onjava/2004/01/14/aop.html)

3. [http://docs.spring.io/spring/docs/2.5.4/reference/aop.html](http://docs.spring.io/spring/docs/2.5.4/reference/aop.html)

Also, I have been talking to my friends and family all week about aspect oriented development. It's been so wonderful.

I was also able to practice it! I just put it on the round-robin schedule that fixed my life and sooner or later it's time came next to NS, OS, Food, DIM, Sleep, DB, TP dissertation, enjoying the wonderful time at University, running for a marathon and the wonderful help I got from my team to further pop things from my schedule so that I have more time for Aspect Oriented Development.

And now, that the week is gone and I got to know (please, not know - understand and appreciate) how to do Aspect Oriented Development which I will need once I start working in the real work, it's time to throw it away and never look back!

How I would do things differently: I am afraid a lot of stuff from Apache weren't looked at all this year. For example the mini Apache Web Server. But who uses that... Also take a look at these and try to squeeze some more into PSD next year: [wikipedia](http://en.wikipedia.org/wiki/List_of_Apache_Software_Foundation_projects)
