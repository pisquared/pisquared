# First week of lectures

My impressions of the first week of 3rd year at University is that this year it will be really busy. We already have assigment sheets and we are jumping straight into the difficult work - no more lazy introductions. Which is good.

Our team project was announced - TweetDesk. It's gonna help journalists mainly to find emerging topics in twitter. Twitter is great, but I also noticed from my ussage that it's not too good to follow too many entitites, since the noise overwhelms the useful information. With our project, we are going to try to fix that.

We also met our project supervisor and the 2 PhD guys who will be helping us. As far as we understood in this early stages, we will be mainly working on the front end. This is great news for me since I am already interested in beautiful and simple design and visualisations. Moreover, I worked on similiar tasks during my summer internship and I have some experience. 

As far as the codebase goes, as far as I am concerned, we should have some backend algorithms for data scraping and event allocation and lots of testing data from historical twitter posts. It is mainly written in Java, but there will also be code from Go, Python and others.

On the PSD lecture, we learned about different design models, some historical like the waterfall, V and spiral and the more recent ones like agile development. I particularly like the agile techniques since it gives you what matters for the whole project and end product and not what matters for some manager who is not involved at all in the project. This makes more sence for me. The team's feelings are also similiar, so we chose to go with this approach for other projects and specifically chose the scrum method. We will try dynamic allocation - i.e. no single individual will be scrum master for the whole process, but rather the scrum master "hat" will change in individual sprints.

So far, so good, let's see how next week will be.
