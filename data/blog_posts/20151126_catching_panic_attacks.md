# "Catching" Panic Attacks

I am experiencing a panic attack right now. It's a second day in a row and I've promised to myself I will be observing closely as I know getting one is never the whole story. They come in bunches. It's a very irrational thing - my heart rate is elevated from seemingly unknown reason, you can actually hear it in your ears, I want to hide, isolate or run away somewhere. Fortunately I've experienced enough of those that I can deny the urge and explore...

## Debugging
People don't come with debuggers. That would be a highly useful thing. Why? 

When writing a computer program sometimes things go wrong. You have introduced a bug. Depending on the programming language and type of bug, the program might "throw an exception", i.e. understand that the program has run into an exceptional condition which is not the "normal flow". For example, when you see an error like this

![blue screen of death](media/20151126_catching_panic_attacks/bsod.jpg)

it means that your Operating system encountered something it couldn't handle. Different languages talk differently about it - throwing/raising an exception/error or ironically, the language I am learning right now (go), calls it **panicking**.

Broadly speaking there are two types of exceptions - ones that you have anticipated and others that you haven't. Anticipated exceptions can be surrounded by some code that handles it. Again, different languages call it catching/handling, go calls it **recovering**. A program can recover from something unexpected, probably notify the user, but overall - it continues working as it should.

For example human society has invented a way to recover from anticipated exceptional conditions like bumping into someone. You naturally anticipate when walking in a crowd that you may hit someone. The way polite people recover from it is to say "Sorry". In Bulgaria many times, we recover by looking evil-y at each other. Doesn't matter, we anticipate, we recover and move on.

However, the unanticipated exceptions can be the tricky ones. You haven't thought of them, sometimes they appear randomly. In that case, you run another program which inspects your program and runs it step-by-step. It's called a **debugger**.

A debugger generally has play/pause/forward buttons which allows you to fine control the execution of a program in human understandable times. A processor is much quicker than a human (in terms of the operations in which the processor is good at - well, duuh) and executes program lines in thousandths or millionths of a second. A human may need a few seconds or several minutes to understand what is happening. So you press the pause button (or set a break point somewhere in the code) and inspect what the program state at this moment is.

Now humans as well as computers handle poorly unanticipated exceptions but the reasons are different. In computers, the programmer hasn't written that piece of code to handle the exception, thus introducing a bug. So the program crashes. It can die, hang or otherwise be a very bad program. Humans respond differently too - they can cry, shout, depress, overeat, become unresponsive or millions of other things. As in programs, sometimes we know what caused it - e.g. your spouse cheated, sometimes we don't.

With my current panic attack, I don't. So if I was a program, I would run a debugger to try to investigate. Unfortunately, in humans, you can hardly be a rational debugger while the exceptional situation is appearing.

## Debugging for humans
But I will do what a debugger does - write as much as I can about my state at this moment and what led to this state trying not to judge as to why, because I know I won't be rational. In programming terms, I will just dump all I know about myself right now and try to analyse it later.

Stack trace as the panic started occurring:
* Travelling to my part time job as a research assistant at the University of Glasgow. I was reading hacker news and particularly this [article about startups](http://macro.ycombinator.com/articles/2015/11/jessica-livingston-startup-school-radio/) 
* I've been awake for 1.5 hours, basically packing and commuting. 
* I've slept relatively well compared to previous 4 nights (Android sleep tracker helps me quantify this - 5.5 hours compared to about 3.5 of deep sleep in the last 4 nights previously).
* I've been more optimistic about the start up than usual in the past week, working hard on doing it. Evident by the number of commits, lines of code and subjective attitude.
* My appetite is down in the past 3 days.
* I have been poorly exercising in the past two weeks.
* Broke up my 2-months relationship two days ago.

## Handling panic attacks
I have some tricks to handle panic attacks when they come. The first time I had one, I didn't have a strategy. Other than...well... I called an ambulance at 2 in the morning and scared the shit out of my room mate that I was dying. If you've never experienced one, it's pretty intense. Especially when you don't have the knowledge of what is happening, you feel you might be getting a heart attack. It probably doesn't make sense when you are 22 and overall healthy but you don't think that in this moment. 

I laugh at it now but I was probably most scared than ever in my life back then. It seems funny from the outside and when I had subsequent ones in the coming months, my best friends joked that I was dying. Which was actually cool because it helped me diminish the effect of how "serious" the situation was. Best friends always know best.

So my first physical tactic is to slow down my breathing as unnatural as it feels in the moment. Count to 5 while breathing, hold breath for 5 and exhale for 5. Repeat 5 times. Reevaluate.

It looks stupid, mumbo-jumbo woodoo stuff, but physical stuff help the mind. It usually calms me down a bit so that I can see there is nothing too serious to worry about. But in a case of a panic attack it doesn't always work too well. However, at least I know what it is and given time (about half an hour) I can just let it pass without calling an ambulance :)

So now that I've been writing in the past half an hour and evaluating what is happening in my life, it makes it easier to call my rational part.

## State Analysis
The part time job at the University is a job that I wish I didn't have but have to have since I'm working on a start-up which doesn't generate revenue so far. I've been working in a start up world for about a year now, pivoting, exploring and there are whole lot of reasons of why it hadn't worked so far. From the "stack trace" I can see that stress has been accumulating without being released properly in the past few weeks (releasing for me usually means exercising). However, I've had other periods with no proper exercises and similar state. The low appetite is very unnatural for me but I guess I can also attribute it to stress. The curious part is probably the break up.

The last time I had severe panic attacks is when I broke up with my previous girlfriend. The break up then was of a 2.5 year relationship and it was a bit longer than an abrupt "we're done" kind of way but I don't want to go into details.

The thing with break ups is that you usually feel they are coming at least for me. Could that be the thing? And if yes, why does it cause panic attacks for me?

I said right after the break up to a friend of mine that I usually expect to feel much worse after a break up but I'm usually quite okay compared to my expectations. Well, maybe I feel that way, but I'm not.

I don't fall into too deep depressions - usually a good night sleep heals everything in me. I see myself as a rather optimistic person and take stuff lightly. However it could be the case that my way of handling exceptions is not through depression but through anxiety. It's not news for me - I usually fight this with tons of exercising, good diet and friends. So I'm going to force myself for the next two weeks to put this as a higher priority in my list and exercise every day. I've done [this multiple times before](http://pi2squared.blogspot.co.uk/2012/09/blog-post.html), it works for me.

### So what?
This blog post is rather more personal than usual. I felt like talking about it here because I feel quite comfortable about the problem that I have and could help other people in a situation similar to mine. Also, I like writing. I bet there are a lot of wanna-be-preneurs who have a lot of stress in their lives and can relate to the debugger metaphor.

By the way, here's a free idea for anyone who complains there are not enough ideas to work on, with time and desire to implement. [CC0](https://creativecommons.org/publicdomain/zero/1.0/) from this line below:

We need debuggers for humans. But not like this above pseudo-psychological analysis that works for me but might not work for anybody else. A real, metric based debugger that can take a stack trace of the current mind with as many variables as it can, dump it somewhere and then a rational person can explore and analyse. I guess you also need to track yourself in "normal state". I'm becoming a fan of tracking stuff with that sleep tracking thing and I haven't explored the market too much but I feel it's pretty new market and hasn't been explored too much yet. The trackers and the "quantify yourself" movement that we are experiencing with smart watches and the like is just the beginning. People will always want to know more about themselves in as many possible ways - physically and mentally. Nowadays the "obvious" pulse, location, movement, sleep tracking are becoming a standard. They might not even be exploited completely. How about more subtle or harder to measure variables like cortisol levels, brain activity and others? Technologies like portable blood laboratories are in the exploratory stage for medical professionals. How about for the public?

Anyone up for a start up?
