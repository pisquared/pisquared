# Bullsh8t, Episode #1 - Astrology

*This episode of bulls8t is sponsored by my own desire to limit some of the bullsh8t in the world*

Horoscopes are the worst thing since the invention of God(s). The stupid idea that planets and stars somehow influence your day to day life is idiotic and should have died pretty much whenever it was conceived. Like most of humanity. Or at least ever since we know that it doesn't influence your life. Which in all practical terms has been forever.

### Hey babeee, Which sign are you?
Astrology, a.k.a. horoscopes, a.k.a. these things in the websites you read "just for fun but you actually don't really believe" (but deep down you actually do believe them because you think there is a grand scheme of things, but deeper, deeper down you know you don't know anything) revolves around an idea called a sign. Signs are twelve words that sound funny in English so that's why we stick with much hipster Latin.

![achzually](media/bullsh8t/01_00_actually.png)

ACKCHYUALLY. Signs are constellations in the sky. Constellations are what our pattern matching machines in our heads try to invent whenever they look at pretty much anything. Do an experiment if you don't believe me.

Spill some salt on the table. Now lock your head on the table and watch that for 30 days. In less than 5 minutes you will start seeing triangles and rectangles, also pentagons, hexagons and septagons.

Octagons? Possible.

Nonagons? Sure.

How about shapes with 10 sides. Maybe.

11 sides? I can see that happening.

12 sides? It is entirely possible.

The point is, you will see anything you want to see. And when you are the night shift at some old point in the past and have to be under a table of distant salt (also known as glowing balls of hydrogen and helium) you start seeing shapes. Wanna see how Aries looks like? Well, take a look at this magnificient ram:

![it's alive!](/media/bullsh8t/01_01_aries.jpg)

Most of them constellations are like this. If you go out and look UP rather than at your glowing rectangle of light in your hand, you will see that. Well, no, actually you will see this if you also go somewhere nice and clean like a mountain, not in the dust- and light- poluted cities.

Man, we have screwed up these cities.

Also if there are no white water vapour in the air also known as tobacco smoke that smokers keep on saying "itz nize and calmz ya dawn man". Fuck you.

To help you in your quests, sometimes the civilization hires large laser pointers, shoot them up in the stratosphere and draw lines for you to imagine. Just ask you nearest flat-earther to tell you when and you will see this:

![it's alive!](/media/bullsh8t/01_02_aries_connected.jpg)

So yeah, somehow someone saw a ram there. Yeah.

Moving on. Now why Aries is special collection of dots? Because from our very special point of view (the Earth) the Sun appears to pass through these constellations as the year goes on. And wherever the Sun has been when you were born in relation to other stars that's what sign you are. That's it. Now I took a trip to space, hired a bunch of lasers and satelites to draw the lines just to show you this photo:

![it's alive!](/media/bullsh8t/01_03_sun_in_aries.jpg)

You are welcome.

## The 4 Lies THEY have been feeding us! You won't believe number 2!!
No, I didn't go to space. Obviously. This "photo" is taken in the future, in May of 2019 (or the past depending on when you are reading this crap). And I sure won't use my time machine just for a shity blog post. But that's just the small lie.

The real lie is that the Earth is flat.

But another lie is that if you paid enough attention, May of 2019?! But Aries is in March/April. Then why is the Sun in the constellation in May?

And are there only 12 constellations through which the Sun passes?

And who cares - I mean, if you are reading this, you probably know the answers. People who believe in zodiac crap generally don't read. Well, they do read but only their horoscopes. And facebook posts. But facebook I hear is not cool anymore because it indeed requires reading. Now the hype is in Instafeces where you don't need to read - just look at photos of the Magellanic clouds.

Haha, but no, really, you just look at pictures of the concerning climate change graphs.

Haha, but no seriously you look at pictures of your mom. Haha but she is so fat that, eh... whatever.

Ok, but seriously - there are 13 constellations, and they are not given equal rights. Like not at all, it's worse than Attack helicopter gender and the rest.

![fixd that for ya](/media/bullsh8t/01_05_attack_helicopter.jpg)

Also, 13 is unlucky and people are superstitious so they cut down the Snake bearer Ophiuchus which is between Scorpio and Sagitarius because ain't nobody wants to be a Snake bearer. And also dates defer because the Earth wobbles like a coin on a table (get it, cause it's flat).

Do you want to see what Aries looks like from Betelgeuse, a star far away but still in our galaxy? Here it is:

![still magnificient tho](/media/bullsh8t/01_04_aries_from_betelgeuse.jpg)

You see it now? Great!

### So... what I was trying to say is...
Astrology is full of Tauri excrements. Like seriously full of bullsh8t. If I hear anybody talking that they have read that today it's gonna be a lucky day because they are Pisces, I go away. More than I go away if someone is smoking and when someone is smoking it is actually burning my lungs.

If somebody asks me which sign I am, I tell her she is an idiot (see, gender equality, I use "she" when it's relevant) and I don't waste my time with her. I prefer to waste my time by watching and recording meteors which is another useless thing but oh so much better than listening about horoscopes.






















































