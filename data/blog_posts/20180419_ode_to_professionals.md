# Ode to professionals

A professional doesn't dress well to impress and fake his professionalism. He dresses well to feel professional.

A professional is not hack-y, quick or dirty. He is a thorough and precise.

A professional is not arrogant or know-it-all. He works together with his colleagues, learns from them and helps them.

A professional understands planned work, unplanned work and plans for the unplanned work.

A professional takes responsibility but never blames.

A professional always learns.

A professional uses the right tool for the job. Not the one that is most hyped at the moment, nor the one he is just most familiar with but too complex or too simple for this job.

A professional understands his human limitations. Thus he never overworks to show off or mistreats the rest of his life for his craft.

A professional always tells the truth, voices his opinions where they are relevant to the work and protects his craft and his colleagues.

A professional understands or tries to understand to the best of his abilities the risks and tradeoffs of choosing one solution over another in the broader context of the work being done.
