# Here's how I got my music from Google Music (as of March 2018)

At some point I uploaded all my music on [Google Music](https://play.google.com/music) - they provided 20,000 songs upload for free. Then of course, they started pushing their paid service - fair enought. I tried the trial several times, I paid subscriptions several times over the years, but I never enjoyed the suggestions it was giving me and somehow the music I wanted, wasn't there. So eventually I always just went to YouTube to listen to music.

But now that I am trying to detach a little bit from the cloud and [get my data back](http://pi2squared.blogspot.be/2018/03/getting-back-control-of-my-digital-life.html) I wanted to try to download my music from Google Music.

## Google Checkout

My first attempt was the assumption that I can get my music as a part of the standard Google Get Back My Data or whatever. It was fairly easy to get there - [MyAccount](https://myaccount.google.com/) from my profile image, then [Takeout](https://myaccount.google.com/privacy#takeout) then click on Create Archive.

![google checkout](/media/20180331_heres_how_i_go…usic_from_google_music/google_checkout.png)

All seems good, right? Do you notice the small print?

![google checkout small print - you can't download your music](/media/20180331_heres_how_i_go…usic_from_google_music/checkout_small_print.png)

Ah, I need to use Google Play Music Manager to actually get my music. Otherwise, I just get a metadata. Fine. Following the link doesn't really lead me to downloading the Music manager, just a help page. But I am Smart (or stubborn), I navigated around, found the link to actually [download](https://play.google.com/music/listen?u=0#/manager) it.

## Google Music Manager

![google music manager](/media/20180331_heres_how_i_go…usic_from_google_music/google_music_manager.png)

I install my music manager, sign in and select to Download all my music, specify a folder...

and nothing happens.

I am slightly annoyed but very stubborn so I push it. So let's see what's hapenning.

The .config folder in my home directory contains google-musicmanager and a log. Let's tail it:

```bash
pi2@pi2-home-laptop ~/.config/google-musicmanager $ tail -F google-musicmanager.log

[../Shared/HttpClients/CurlHttpClientImpl.cpp:57 ::CurlSslVerifyCertificate()]
2018-03-31 11:10:08,371 +0200 ERROR TId 0x7f5756183700 curl_easy_perform failed with 60 [../Shared/HttpClients/CurlHttpClientImpl.cpp:402 CurlHttpClientImpl::performPrivate()]
2018-03-31 11:10:08,372 +0200 ERROR TId 0x7f5756183700 Certificate error [../Shared/HttpClients/CurlHttpClientImpl.cpp:406 CurlHttpClientImpl::performPrivate()]
2018-03-31 11:10:08,372 +0200 ERROR TId 0x7f5756183700 Call to https://sj.googleusercontent.com/download?id=[REDACTED]&itag=25&source=skyjam_user_upload&o=[REDACTED]3&uits=1&tid=[REDACTED]&ip=0.0.0.0&ipbits=0&expire=1522487498&sparams=id,itag,source,o,uits,tid,ip,ipbits,expire&signature=[REDACTED]&key=sj3&targetkbps=768 failed with result 3 [../Shared/HttpClients/CommonHttpClientImpl.cpp:50 CommonHttpClientImpl::perform()]
2018-03-31 11:10:08,393 +0200 ERROR TId 0x7f5756183700 Failed to Send Request Result - 3 [../Shared/HttpClients/DownloadFileClient.cpp:220 DownloadFileClient::Execute()]
2018-03-31 11:10:08,397 +0200 ERROR TId 0x7f5756183700 Failed to download song id=[REDACTED] [Core/DownloadProcessor.cpp:128 DownloadProcessor::processDownloadRequest()]
2018-03-31 11:10:08,825 +0200 INFO TId 0x7f5756183700 errno 2 calling stat /home/pi2/.config/google-musicmanager/00 Brett Domino Trio- Ultimate Ch.mp3 [../../Shared/TpnBase/TpnPortC.c:150 ::_Port_GetFileInfo()]
2018-03-31 11:10:08,883 +0200 ERROR TId 0x7f5756183700 Failed to Verify Certificate: /C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.googleusercontent.com (unable to get local issuer certificate:0x00000000) [../Shared/HttpClients/CurlHttpClientImpl.cpp:44 ::CurlSslVerifyCertificate()]
2018-03-31 11:10:08,883 +0200 ERROR TId 0x7f5756183700 Certificate /C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.googleusercontent.com:
-----BEGIN CERTIFICATE-----
...
```

(I redacted some parts of the output, not sure if any of them are sensitive but better safe than sorry).

So it's an expired certificate from what I gather. I guess there might be some vulnerability with this level of unsupported tool but I don't want to do this now. I want my MUSIIIIC.

I am getting a bit more annoyed and a bit more stubborn. Time to fire the hard guns.

## Windows Music manager client

Google must be supporting Windows client better. Obtained a completely legitimate copy of Windows 10, spin up a virtual machine on Virtualbox, download the manager, try to log in and...

![google music manager - 2 step verification](/media/20180331_heres_how_i_go…usic_from_google_music/music_manager_two_step.png)

Huh? Nothing happened. Okay, let's try the Google Authenticator...

![google music manager - authenticator](/media/20180331_heres_how_i_go…usic_from_google_music/music_manager_auth.png)

Okay, I am quite angry now. 

## Manual to the rescue

My only left option seems to be to either try to dual boot and hope Google Music manager detects that it's on a virtual machine and for some /security?/ reason decides not to authenticate me.

Or manually download the songs from the web interface.

I decide manual is going to be quicker - I've got about 2000 songs.

![google music web - 100 limit](/media/20180331_heres_how_i_go…usic_from_google_music/music_hundred_limit.png)

Arghhhh!!! So in a batch of 100 I will need around 20 selections... Should I automate? Let's go to [xkcd](https://xkcd.com/1205/):

![xkcd - is it worth the time](/media/20180331_heres_how_i_go…usic_from_google_music/is_it_worth_the_time.png)

I think I will be better off manually. Besides, inspecting the code seems that for optimization reasons, the song list is not loaded in its completeness but it keeps around 20 songs in the DOM and the rest it dynamically fetches on scroll events.

The good news is, the table keeps a data-index field with consequitive numbers so that I can at least find where I am and download in batch of 100

![dev tools - inspect elements of the table](/media/20180331_heres_how_i_go…usic_from_google_music/dev_tools.png)

Oh, just one more thing - you can download a certain song just 2 times from web and apparently I have already downloaded some before...

![maximum two downloads](/media/20180331_heres_how_i_go…usic_from_google_music/max_two_downloads.png)

I am not too happy with this. But I got my Music back.

C'mon, Google...
Just to put oil in the fire, today Google announced that it's discontinuing [yet another service](https://developers.googleblog.com/2018/03/transitioning-google-url-shortener.html) - goo.gl.
